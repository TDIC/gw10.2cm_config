package rules.Preupdate.ABContactPreupdate_dir.TDIC_WC100000_dir
uses java.util.HashSet
uses gw.pl.logging.LoggerCategory

@gw.rules.RuleName("TDIC_COPU110000 - Contact With Duplicate OfficialIDs")
internal class TDIC_COPU110000ContactWithDuplicateOfficialIDs {
  static function doCondition(aBContact : entity.ABContact) : boolean {
/*start00rule*/
return aBContact.OfficialIDs != null && !aBContact.OfficialIDs.IsEmpty
/*end00rule*/
}

  static function doAction(aBContact : entity.ABContact, actions : gw.rules.Action) {
/*start00rule*/
/**
 * US1161 - Synchronize OfficialIDs at ContactManager Level
 * 3/13/2015 Alvin Lee
 *
 * Remove duplicates for new contacts that will get sent when a message retryable error occurs or when the message queue
 * is suspended.
 */
var nonDuplicatedSet = new HashSet<ABOfficialID>()
for (officialID in aBContact.OfficialIDs) {
  if (!nonDuplicatedSet.hasMatch( \ nonDuplicatedID -> officialID.OfficialIDType == nonDuplicatedID.OfficialIDType)) {
    nonDuplicatedSet.add(officialID)
    LoggerCategory.RULES.debug("ABContact '" +aBContact.DisplayName + "' has non-duplicated Official ID of Type '"
        + officialID.OfficialIDType + "' and Value '" + officialID.OfficialIDValue + "'")
  }
  else {
    LoggerCategory.RULES.debug("ABContact '" +aBContact.DisplayName + "' has duplicated Official ID of Type '"
        + officialID.OfficialIDType + "' and Value '" + officialID.OfficialIDValue + "'. Discarding.")
  }
}
aBContact.OfficialIDs = nonDuplicatedSet.toTypedArray()
/*end00rule*/
  }
}
