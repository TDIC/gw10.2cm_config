package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ContactSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 36, column 45
    function def_onEnter_10 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(SearchCriteria, isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 36, column 45
    function def_onEnter_12 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(SearchCriteria, isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 40, column 49
    function def_onEnter_16 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(SearchCriteria, not isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 40, column 49
    function def_onEnter_18 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(SearchCriteria, not isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 157, column 54
    function def_onEnter_92 (def :  pcf.AddressSearchInputSet) : void {
      def.onEnter(SearchCriteria)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 162, column 74
    function def_onEnter_95 (def :  pcf.ContactSearchProximityInputSet) : void {
      def.onEnter(SearchCriteria, proximitySearchPageHelper)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 166, column 41
    function def_onEnter_97 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 36, column 45
    function def_refreshVariables_11 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(SearchCriteria, isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 36, column 45
    function def_refreshVariables_13 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(SearchCriteria, isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 40, column 49
    function def_refreshVariables_17 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(SearchCriteria, not isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 40, column 49
    function def_refreshVariables_19 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(SearchCriteria, not isPerson(SearchCriteria)))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 157, column 54
    function def_refreshVariables_93 (def :  pcf.AddressSearchInputSet) : void {
      def.refreshVariables(SearchCriteria)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 162, column 74
    function def_refreshVariables_96 (def :  pcf.ContactSearchProximityInputSet) : void {
      def.refreshVariables(SearchCriteria, proximitySearchPageHelper)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 166, column 41
    function def_refreshVariables_98 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.ContactSubtype = (__VALUE_TO_SET as typekey.ABContact)
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ContactSearchDV.pcf: line 47, column 41
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=OrganizationName_Input) at ContactSearchDV.pcf: line 55, column 52
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.OrganizationName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=AttorneySpecialty_Input) at ContactSearchDV.pcf: line 64, column 47
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.AttorneySpecialty = (__VALUE_TO_SET as typekey.LegalSpecialty)
    }
    
    // 'value' attribute on TypeKeyInput (id=DoctorSpecialty_Input) at ContactSearchDV.pcf: line 73, column 46
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.DoctorSpecialty = (__VALUE_TO_SET as typekey.SpecialtyType)
    }
    
    // 'value' attribute on TypeKeyInput (id=AdjudicativeDomain_Input) at ContactSearchDV.pcf: line 82, column 51
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.AdjudicativeDomain = (__VALUE_TO_SET as typekey.AdjudicativeDomain)
    }
    
    // 'value' attribute on TypeKeyInput (id=MedicalSpecialty_Input) at ContactSearchDV.pcf: line 91, column 46
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.MedicalOrgSpecialty = (__VALUE_TO_SET as typekey.SpecialtyType)
    }
    
    // 'value' attribute on TypeKeyInput (id=LawFirmSpecialty_Input) at ContactSearchDV.pcf: line 100, column 47
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.LawFirmSpecialty = (__VALUE_TO_SET as typekey.LegalSpecialty)
    }
    
    // 'value' attribute on TextInput (id=EmployeeNumber_Input) at ContactSearchDV.pcf: line 108, column 50
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.EmployeeNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=PreferredVendors_Input) at ContactSearchDV.pcf: line 116, column 52
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.PreferredVendors = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=MinimumScore_Input) at ContactSearchDV.pcf: line 124, column 40
    function defaultSetter_67 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.Score = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=Tags_Input) at ContactSearchDV.pcf: line 133, column 47
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.TagTypes = (__VALUE_TO_SET as typekey.ContactTagType[])
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllTagsRequired_Input) at ContactSearchDV.pcf: line 140, column 49
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.AllTagsRequired = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=VendorAvailability_Input) at ContactSearchDV.pcf: line 146, column 53
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.VendorAvailability = (__VALUE_TO_SET as typekey.VendorAvailabilityType)
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludePendingCreates_Input) at ContactSearchDV.pcf: line 151, column 51
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchSpec.IncludePendingCreates = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'filter' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function filter_4 (VALUE :  typekey.ABContact, VALUES :  typekey.ABContact[]) : java.lang.Boolean {
      return VALUE != typekey.ABContact.TC_ABAUTOREPAIRSHOP and VALUE != typekey.ABContact.TC_ABAUTOTOWINGAGCY 
    }
    
    // 'mode' attribute on InputSetRef at ContactSearchDV.pcf: line 36, column 45
    function mode_14 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'onChange' attribute on PostOnChange at ContactSearchDV.pcf: line 31, column 64
    function onChange_0 () : void {
      gw.api.util.SearchUtil.resetResultOnly()
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function valueRange_5 () : java.lang.Object {
      return SearchCriteria.getAvailableSubtypes(requiredContactType)
    }
    
    // 'valueRange' attribute on RangeInput (id=MinimumScore_Input) at ContactSearchDV.pcf: line 124, column 40
    function valueRange_69 () : java.lang.Object {
      return scoreRange()
    }
    
    // 'valueRange' attribute on RangeInput (id=Tags_Input) at ContactSearchDV.pcf: line 133, column 47
    function valueRange_76 () : java.lang.Object {
      return ContactTagType.getTypeKeys(false)
    }
    
    // 'value' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function valueRoot_3 () : java.lang.Object {
      return SearchCriteria
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludePendingCreates_Input) at ContactSearchDV.pcf: line 151, column 51
    function valueRoot_90 () : java.lang.Object {
      return searchSpec
    }
    
    // 'value' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function value_1 () : typekey.ABContact {
      return SearchCriteria.ContactSubtype
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ContactSearchDV.pcf: line 47, column 41
    function value_21 () : java.lang.String {
      return SearchCriteria.TaxID
    }
    
    // 'value' attribute on TextInput (id=OrganizationName_Input) at ContactSearchDV.pcf: line 55, column 52
    function value_26 () : java.lang.String {
      return SearchCriteria.OrganizationName
    }
    
    // 'value' attribute on TypeKeyInput (id=AttorneySpecialty_Input) at ContactSearchDV.pcf: line 64, column 47
    function value_31 () : typekey.LegalSpecialty {
      return SearchCriteria.AttorneySpecialty
    }
    
    // 'value' attribute on TypeKeyInput (id=DoctorSpecialty_Input) at ContactSearchDV.pcf: line 73, column 46
    function value_36 () : typekey.SpecialtyType {
      return SearchCriteria.DoctorSpecialty
    }
    
    // 'value' attribute on TypeKeyInput (id=AdjudicativeDomain_Input) at ContactSearchDV.pcf: line 82, column 51
    function value_41 () : typekey.AdjudicativeDomain {
      return SearchCriteria.AdjudicativeDomain
    }
    
    // 'value' attribute on TypeKeyInput (id=MedicalSpecialty_Input) at ContactSearchDV.pcf: line 91, column 46
    function value_46 () : typekey.SpecialtyType {
      return SearchCriteria.MedicalOrgSpecialty
    }
    
    // 'value' attribute on TypeKeyInput (id=LawFirmSpecialty_Input) at ContactSearchDV.pcf: line 100, column 47
    function value_51 () : typekey.LegalSpecialty {
      return SearchCriteria.LawFirmSpecialty
    }
    
    // 'value' attribute on TextInput (id=EmployeeNumber_Input) at ContactSearchDV.pcf: line 108, column 50
    function value_56 () : java.lang.String {
      return SearchCriteria.EmployeeNumber
    }
    
    // 'value' attribute on BooleanRadioInput (id=PreferredVendors_Input) at ContactSearchDV.pcf: line 116, column 52
    function value_61 () : java.lang.Boolean {
      return SearchCriteria.PreferredVendors
    }
    
    // 'value' attribute on RangeInput (id=MinimumScore_Input) at ContactSearchDV.pcf: line 124, column 40
    function value_66 () : java.lang.Integer {
      return SearchCriteria.Score
    }
    
    // 'value' attribute on RangeInput (id=Tags_Input) at ContactSearchDV.pcf: line 133, column 47
    function value_73 () : typekey.ContactTagType[] {
      return SearchCriteria.TagTypes
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllTagsRequired_Input) at ContactSearchDV.pcf: line 140, column 49
    function value_80 () : java.lang.Boolean {
      return SearchCriteria.AllTagsRequired
    }
    
    // 'value' attribute on TypeKeyInput (id=VendorAvailability_Input) at ContactSearchDV.pcf: line 146, column 53
    function value_84 () : typekey.VendorAvailabilityType {
      return SearchCriteria.VendorAvailability
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludePendingCreates_Input) at ContactSearchDV.pcf: line 151, column 51
    function value_88 () : java.lang.Boolean {
      return searchSpec.IncludePendingCreates
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function verifyValueRangeIsAllowedType_6 ($$arg :  typekey.ABContact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MinimumScore_Input) at ContactSearchDV.pcf: line 124, column 40
    function verifyValueRangeIsAllowedType_70 ($$arg :  java.lang.Integer[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MinimumScore_Input) at ContactSearchDV.pcf: line 124, column 40
    function verifyValueRangeIsAllowedType_70 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Tags_Input) at ContactSearchDV.pcf: line 133, column 47
    function verifyValueRangeIsAllowedType_77 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Tags_Input) at ContactSearchDV.pcf: line 133, column 47
    function verifyValueRangeIsAllowedType_77 ($$arg :  typekey.ContactTagType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactSubtype_Input) at ContactSearchDV.pcf: line 28, column 39
    function verifyValueRange_7 () : void {
      var __valueRangeArg = SearchCriteria.getAvailableSubtypes(requiredContactType)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=MinimumScore_Input) at ContactSearchDV.pcf: line 124, column 40
    function verifyValueRange_71 () : void {
      var __valueRangeArg = scoreRange()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_70(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Tags_Input) at ContactSearchDV.pcf: line 133, column 47
    function verifyValueRange_78 () : void {
      var __valueRangeArg = ContactTagType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_77(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSetRef at ContactSearchDV.pcf: line 40, column 49
    function visible_15 () : java.lang.Boolean {
      return not isPerson(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 42, column 145
    function visible_25 () : java.lang.Boolean {
      return isCompany(SearchCriteria) or (isPerson(SearchCriteria) and !isUserContact(SearchCriteria) and !isAdjudicator(SearchCriteria))
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 50, column 115
    function visible_30 () : java.lang.Boolean {
      return isPerson(SearchCriteria) and !(isPersonVendor(SearchCriteria) or isUserContact(SearchCriteria))
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 58, column 46
    function visible_35 () : java.lang.Boolean {
      return isAttorney(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 67, column 44
    function visible_40 () : java.lang.Boolean {
      return isDoctor(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 76, column 49
    function visible_45 () : java.lang.Boolean {
      return isAdjudicator(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 85, column 52
    function visible_50 () : java.lang.Boolean {
      return isMedicalCareOrg(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 94, column 45
    function visible_55 () : java.lang.Boolean {
      return isLawFirm(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 103, column 49
    function visible_60 () : java.lang.Boolean {
      return isUserContact(SearchCriteria)
    }
    
    // 'visible' attribute on InputSet at ContactSearchDV.pcf: line 111, column 44
    function visible_65 () : java.lang.Boolean {
      return isVendor(SearchCriteria)
    }
    
    // 'visible' attribute on InputSetRef at ContactSearchDV.pcf: line 36, column 45
    function visible_9 () : java.lang.Boolean {
      return isPerson(SearchCriteria)
    }
    
    // 'visible' attribute on InputSetRef at ContactSearchDV.pcf: line 162, column 74
    function visible_94 () : java.lang.Boolean {
      return proximitySearchPageHelper.useGeocodeUIinAddressBook()
    }
    
    property get SearchCriteria () : ABContactSearchCriteria {
      return getRequireValue("SearchCriteria", 0) as ABContactSearchCriteria
    }
    
    property set SearchCriteria ($arg :  ABContactSearchCriteria) {
      setRequireValue("SearchCriteria", 0, $arg)
    }
    
    property get proximitySearchPageHelper () : gw.api.contact.ProximitySearchPageHelper {
      return getRequireValue("proximitySearchPageHelper", 0) as gw.api.contact.ProximitySearchPageHelper
    }
    
    property set proximitySearchPageHelper ($arg :  gw.api.contact.ProximitySearchPageHelper) {
      setRequireValue("proximitySearchPageHelper", 0, $arg)
    }
    
    property get requiredContactType () : Type {
      return getRequireValue("requiredContactType", 0) as Type
    }
    
    property set requiredContactType ($arg :  Type) {
      setRequireValue("requiredContactType", 0, $arg)
    }
    
    property get searchSpec () : gw.api.webservice.addressbook.contactapi.ABContactSearchSpecWithoutPaging {
      return getRequireValue("searchSpec", 0) as gw.api.webservice.addressbook.contactapi.ABContactSearchSpecWithoutPaging
    }
    
    property set searchSpec ($arg :  gw.api.webservice.addressbook.contactapi.ABContactSearchSpecWithoutPaging) {
      setRequireValue("searchSpec", 0, $arg)
    }
    
    function isAdjudicator(c : ABContactSearchCriteria) : boolean { return entity.ABAdjudicator.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isAttorney(c : ABContactSearchCriteria) : boolean { return entity.ABAttorney.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isCompany(c : ABContactSearchCriteria) : boolean { return entity.ABCompany.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isDoctor(c : ABContactSearchCriteria) : boolean { return entity.ABDoctor.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isLawFirm(c : ABContactSearchCriteria) : boolean { return entity.ABLawFirm.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isMedicalCareOrg(c : ABContactSearchCriteria) : boolean { return entity.ABMedicalCareOrg.Type.isAssignableFrom( c.ContactSubtypeType )}
          function isPerson(c : ABContactSearchCriteria) : boolean { return entity.ABPerson.Type.isAssignableFrom( c.ContactSubtypeType )}
          function isPersonVendor(c : ABContactSearchCriteria) : boolean { return entity.ABPersonVendor.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isPlace(c : ABContactSearchCriteria) : boolean { return entity.ABPlace.Type.isAssignableFrom( c.ContactSubtypeType )}
          function isUserContact(c : ABContactSearchCriteria) : boolean { return entity.ABUserContact.Type.isAssignableFrom(c.ContactSubtypeType )}
          function isVendor(c: ABContactSearchCriteria) : boolean { return (entity.ABCompanyVendor.Type.isAssignableFrom(c.ContactSubtypeType ) 
                                                                    or entity.ABPersonVendor.Type.isAssignableFrom( c.ContactSubtypeType )) }
          function scoreRange() : java.lang.Integer[] {
            return { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90}
          }
          function getCountryCode(country : Country) : String {
            if (country == null) {
              return gw.api.admin.BaseAdminUtil.getDefaultCountry().Code;
            }
            return country.Code;
          }
    
    
  }
  
  
}