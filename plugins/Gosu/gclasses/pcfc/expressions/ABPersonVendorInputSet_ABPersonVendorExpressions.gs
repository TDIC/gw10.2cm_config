package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/basics/ABPersonVendorInputSet.ABPersonVendor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ABPersonVendorInputSet_ABPersonVendorExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/basics/ABPersonVendorInputSet.ABPersonVendor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ABPersonVendorInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionEnabled' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 11, column 172
    function actionEnabled_21 () : java.lang.Boolean {
      return person.PrimaryContact != null
    }
    
    // 'action' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 14, column 225
    function action_14 () : void {
      ABContactSearchPopup.push(entity.ABPerson)
    }
    
    // 'action' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 11, column 172
    function action_16 () : void {
      ABContactDetailPopup.push(person.PrimaryContact)
    }
    
    // 'action' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 14, column 225
    function action_dest_15 () : pcf.api.Destination {
      return pcf.ABContactSearchPopup.createDestination(entity.ABPerson)
    }
    
    // 'action' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 11, column 172
    function action_dest_17 () : pcf.api.Destination {
      return pcf.ABContactDetailPopup.createDestination(person.PrimaryContact)
    }
    
    // 'def' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 12, column 171
    function def_onEnter_11 (def :  pcf.NewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(entity.ABPerson, person)
    }
    
    // 'def' attribute on InputSetRef (id=Work) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 38, column 18
    function def_onEnter_25 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person, ABContact#WorkPhone), DisplayKey.get("Web.ContactDetail.Phone.Work"), false))
    }
    
    // 'def' attribute on InputSetRef (id=Home) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 42, column 18
    function def_onEnter_27 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person,ABPerson#HomePhone), DisplayKey.get("Web.ContactDetail.Phone.Home"), false))
    }
    
    // 'def' attribute on InputSetRef (id=Cell) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 46, column 18
    function def_onEnter_29 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person, ABPerson#CellPhone), DisplayKey.get("Web.ContactDetail.Phone.Cell"), false))
    }
    
    // 'def' attribute on InputSetRef (id=Fax) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 50, column 17
    function def_onEnter_31 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person, ABPerson#FaxPhone), DisplayKey.get("Web.ContactDetail.Phone.Fax"), false))
    }
    
    // 'def' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 12, column 171
    function def_refreshVariables_12 (def :  pcf.NewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(entity.ABPerson, person)
    }
    
    // 'def' attribute on InputSetRef (id=Work) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 38, column 18
    function def_refreshVariables_26 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person, ABContact#WorkPhone), DisplayKey.get("Web.ContactDetail.Phone.Work"), false))
    }
    
    // 'def' attribute on InputSetRef (id=Home) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 42, column 18
    function def_refreshVariables_28 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person,ABPerson#HomePhone), DisplayKey.get("Web.ContactDetail.Phone.Home"), false))
    }
    
    // 'def' attribute on InputSetRef (id=Cell) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 46, column 18
    function def_refreshVariables_30 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person, ABPerson#CellPhone), DisplayKey.get("Web.ContactDetail.Phone.Cell"), false))
    }
    
    // 'def' attribute on InputSetRef (id=Fax) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 50, column 17
    function def_refreshVariables_32 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(person, ABPerson#FaxPhone), DisplayKey.get("Web.ContactDetail.Phone.Fax"), false))
    }
    
    // 'value' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 11, column 172
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.PrimaryContact = (__VALUE_TO_SET as entity.ABContact)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 15, column 53
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      (person as ABPersonVendor).Preferred = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 57, column 45
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.PrimaryPhone = (__VALUE_TO_SET as typekey.PrimaryPhoneType)
    }
    
    // 'value' attribute on TextInput (id=Email1_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 66, column 37
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Email2_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 72, column 37
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.EmailAddress2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 81, column 29
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      person.TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=W9received_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 86, column 54
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      (person as ABPersonVendor).W9Received = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=W9receivedDate_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 91, column 58
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      (person as ABPersonVendor).W9ReceivedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=W9ValidFrom_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 96, column 55
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      (person as ABPersonVendor).W9ValidFrom = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=W9ValidTo_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 101, column 53
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      (person as ABPersonVendor).W9ValidTo = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 20, column 55
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      (person as ABPersonVendor).VendorNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 15, column 53
    function editable_0 () : java.lang.Boolean {
      return perm.ABContact.createpreferred
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=SSN_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 81, column 29
    function encryptionExpression_50 (VALUE :  java.lang.String) : java.lang.String {
      return person.maskTaxId(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at ABPersonVendorInputSet.ABPersonVendor.pcf: line 22, column 114
    function onChange_6 () : void {
      new gw.web.ContactDetailsVendorHelper(person).resetVendorIDFlag((person as ABPersonVendor)) 
    }
    
    // 'validationExpression' attribute on TypeKeyInput (id=PrimaryPhone_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 57, column 45
    function validationExpression_33 () : java.lang.Object {
                       if (person.PrimaryPhone == null or                     (person.PrimaryPhone == TC_WORK and person.WorkPhone != null) or                     (person.PrimaryPhone == TC_HOME and person.HomePhone != null) or                     (person.PrimaryPhone == TC_MOBILE and person.CellPhone != null)) {                   return null;                 } else {                   return DisplayKey.get("Web.ContactDetail.Phone.PrimaryPhone.Error");                 }
    }
    
    // 'value' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 11, column 172
    function valueRoot_20 () : java.lang.Object {
      return person
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 15, column 53
    function valueRoot_3 () : java.lang.Object {
      return (person as ABPersonVendor)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 15, column 53
    function value_1 () : java.lang.Boolean {
      return (person as ABPersonVendor).Preferred
    }
    
    // 'value' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 11, column 172
    function value_18 () : entity.ABContact {
      return person.PrimaryContact
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 57, column 45
    function value_34 () : typekey.PrimaryPhoneType {
      return person.PrimaryPhone
    }
    
    // 'value' attribute on TextInput (id=Email1_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 66, column 37
    function value_39 () : java.lang.String {
      return person.EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=Email2_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 72, column 37
    function value_43 () : java.lang.String {
      return person.EmailAddress2
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 81, column 29
    function value_47 () : java.lang.String {
      return person.TaxID
    }
    
    // 'value' attribute on BooleanRadioInput (id=W9received_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 86, column 54
    function value_52 () : java.lang.Boolean {
      return (person as ABPersonVendor).W9Received
    }
    
    // 'value' attribute on DateInput (id=W9receivedDate_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 91, column 58
    function value_56 () : java.util.Date {
      return (person as ABPersonVendor).W9ReceivedDate
    }
    
    // 'value' attribute on DateInput (id=W9ValidFrom_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 96, column 55
    function value_60 () : java.util.Date {
      return (person as ABPersonVendor).W9ValidFrom
    }
    
    // 'value' attribute on DateInput (id=W9ValidTo_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 101, column 53
    function value_64 () : java.util.Date {
      return (person as ABPersonVendor).W9ValidTo
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ABPersonVendorInputSet.ABPersonVendor.pcf: line 20, column 55
    function value_7 () : java.lang.String {
      return (person as ABPersonVendor).VendorNumber
    }
    
    // 'visible' attribute on ABContactInput (id=PrimaryContact_Input) at ABContactWidget.xml: line 14, column 225
    function visible_13 () : java.lang.Boolean {
      return "ABContactSearchPopup.push(entity.ABPerson)" != "" && true
    }
    
    property get person () : ABPerson {
      return getRequireValue("person", 0) as ABPerson
    }
    
    property set person ($arg :  ABPerson) {
      setRequireValue("person", 0, $arg)
    }
    
    
  }
  
  
}