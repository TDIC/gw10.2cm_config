package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/basics/ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ABCompanyVendorBasicInputSet_ABCompanyVendorExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/basics/ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ABCompanyVendorBasicInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 19, column 55
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABCompanyVendor).Preferred = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 24, column 36
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.VendorNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 19, column 55
    function editable_1 () : java.lang.Boolean {
      return perm.ABContact.createpreferred
    }
    
    // 'initialValue' attribute on Variable at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 14, column 49
    function initialValue_0 () : gw.web.ContactDetailsVendorHelper {
      return new gw.web.ContactDetailsVendorHelper(contact)
    }
    
    // 'onChange' attribute on PostOnChange at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 26, column 76
    function onChange_7 () : void {
      contactDetailsVendorHelper.resetVendorIDFlag(contact) 
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 24, column 36
    function valueRoot_10 () : java.lang.Object {
      return contact
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 19, column 55
    function valueRoot_4 () : java.lang.Object {
      return (contact as ABCompanyVendor)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsPreferredVendor_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 19, column 55
    function value_2 () : java.lang.Boolean {
      return (contact as ABCompanyVendor).Preferred
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ABCompanyVendorBasicInputSet.ABCompanyVendor.pcf: line 24, column 36
    function value_8 () : java.lang.String {
      return contact.VendorNumber
    }
    
    property get contact () : ABContact {
      return getRequireValue("contact", 0) as ABContact
    }
    
    property set contact ($arg :  ABContact) {
      setRequireValue("contact", 0, $arg)
    }
    
    property get contactDetailsVendorHelper () : gw.web.ContactDetailsVendorHelper {
      return getVariableValue("contactDetailsVendorHelper", 0) as gw.web.ContactDetailsVendorHelper
    }
    
    property set contactDetailsVendorHelper ($arg :  gw.web.ContactDetailsVendorHelper) {
      setVariableValue("contactDetailsVendorHelper", 0, $arg)
    }
    
    
  }
  
  
}