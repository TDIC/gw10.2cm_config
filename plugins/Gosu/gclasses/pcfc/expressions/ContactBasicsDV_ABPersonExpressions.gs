package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contacts/basics/ContactBasicsDV.ABPerson.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactBasicsDV_ABPersonExpressions {
  @javax.annotation.Generated("config/web/pcf/contacts/basics/ContactBasicsDV.ABPerson.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactBasicsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionEnabled' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 11, column 172
    function actionEnabled_107 () : java.lang.Boolean {
      return (contact as ABPerson).Guardian != null
    }
    
    // 'actionEnabled' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 11, column 172
    function actionEnabled_156 () : java.lang.Boolean {
      return (contact as ABPerson).Employer != null
    }
    
    // 'action' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 14, column 225
    function action_100 () : void {
      ABContactSearchPopup.push(entity.ABPerson)
    }
    
    // 'action' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 11, column 172
    function action_102 () : void {
      ABContactDetailPopup.push((contact as ABPerson).Guardian)
    }
    
    // 'action' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 14, column 225
    function action_149 () : void {
      ABContactSearchPopup.push(statictypeof ((contact as ABPerson).Employer))
    }
    
    // 'action' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 11, column 172
    function action_151 () : void {
      ABContactDetailPopup.push((contact as ABPerson).Employer)
    }
    
    // 'action' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 14, column 225
    function action_dest_101 () : pcf.api.Destination {
      return pcf.ABContactSearchPopup.createDestination(entity.ABPerson)
    }
    
    // 'action' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 11, column 172
    function action_dest_103 () : pcf.api.Destination {
      return pcf.ABContactDetailPopup.createDestination((contact as ABPerson).Guardian)
    }
    
    // 'action' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 14, column 225
    function action_dest_150 () : pcf.api.Destination {
      return pcf.ABContactSearchPopup.createDestination(statictypeof ((contact as ABPerson).Employer))
    }
    
    // 'action' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 11, column 172
    function action_dest_152 () : pcf.api.Destination {
      return pcf.ABContactDetailPopup.createDestination((contact as ABPerson).Employer)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 34, column 56
    function def_onEnter_10 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.api.name.ContactNameOwner(new gw.api.name.ABPersonNameDelegate(contact as ABPerson)))
    }
    
    // 'def' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 12, column 171
    function def_onEnter_146 (def :  pcf.NewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof ((contact as ABPerson).Employer), contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 241, column 33
    function def_onEnter_162 (def :  pcf.ABPersonVendorSpecialtyInputSet_ABAttorney) : void {
      def.onEnter(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 241, column 33
    function def_onEnter_164 (def :  pcf.ABPersonVendorSpecialtyInputSet_ABDoctor) : void {
      def.onEnter(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 241, column 33
    function def_onEnter_166 (def :  pcf.ABPersonVendorSpecialtyInputSet_default) : void {
      def.onEnter(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 244, column 33
    function def_onEnter_169 (def :  pcf.ABPersonVendorInputSet_ABPersonVendor) : void {
      def.onEnter(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 244, column 33
    function def_onEnter_171 (def :  pcf.ABPersonVendorInputSet_default) : void {
      def.onEnter(contact as ABPerson)
    }
    
    // 'def' attribute on ListViewInput at ContactBasicsDV.ABPerson.pcf: line 279, column 27
    function def_onEnter_178 (def :  pcf.ContactEFTLV) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 50, column 38
    function def_onEnter_22 (def :  pcf.TagsInputSet) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 70, column 62
    function def_onEnter_37 (def :  pcf.PrimaryAddressInputSet) : void {
      def.onEnter((contact as ABPerson))
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 117, column 33
    function def_onEnter_54 (def :  pcf.ABUserContactBasicsInputSet_ABUserContact) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 117, column 33
    function def_onEnter_56 (def :  pcf.ABUserContactBasicsInputSet_default) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 120, column 33
    function def_onEnter_59 (def :  pcf.ABAdjudicatorBasicsInputSet_ABAdjudicator) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 120, column 33
    function def_onEnter_61 (def :  pcf.ABAdjudicatorBasicsInputSet_default) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 34, column 56
    function def_onEnter_8 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.ContactNameOwner(new gw.api.name.ABPersonNameDelegate(contact as ABPerson)))
    }
    
    // 'def' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 12, column 171
    function def_onEnter_97 (def :  pcf.NewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(entity.ABPerson, contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 34, column 56
    function def_refreshVariables_11 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.ContactNameOwner(new gw.api.name.ABPersonNameDelegate(contact as ABPerson)))
    }
    
    // 'def' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 12, column 171
    function def_refreshVariables_147 (def :  pcf.NewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof ((contact as ABPerson).Employer), contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 241, column 33
    function def_refreshVariables_163 (def :  pcf.ABPersonVendorSpecialtyInputSet_ABAttorney) : void {
      def.refreshVariables(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 241, column 33
    function def_refreshVariables_165 (def :  pcf.ABPersonVendorSpecialtyInputSet_ABDoctor) : void {
      def.refreshVariables(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 241, column 33
    function def_refreshVariables_167 (def :  pcf.ABPersonVendorSpecialtyInputSet_default) : void {
      def.refreshVariables(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 244, column 33
    function def_refreshVariables_170 (def :  pcf.ABPersonVendorInputSet_ABPersonVendor) : void {
      def.refreshVariables(contact as ABPerson)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 244, column 33
    function def_refreshVariables_172 (def :  pcf.ABPersonVendorInputSet_default) : void {
      def.refreshVariables(contact as ABPerson)
    }
    
    // 'def' attribute on ListViewInput at ContactBasicsDV.ABPerson.pcf: line 279, column 27
    function def_refreshVariables_179 (def :  pcf.ContactEFTLV) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 50, column 38
    function def_refreshVariables_23 (def :  pcf.TagsInputSet) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 70, column 62
    function def_refreshVariables_38 (def :  pcf.PrimaryAddressInputSet) : void {
      def.refreshVariables((contact as ABPerson))
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 117, column 33
    function def_refreshVariables_55 (def :  pcf.ABUserContactBasicsInputSet_ABUserContact) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 117, column 33
    function def_refreshVariables_57 (def :  pcf.ABUserContactBasicsInputSet_default) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 120, column 33
    function def_refreshVariables_60 (def :  pcf.ABAdjudicatorBasicsInputSet_ABAdjudicator) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 120, column 33
    function def_refreshVariables_62 (def :  pcf.ABAdjudicatorBasicsInputSet_default) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 34, column 56
    function def_refreshVariables_9 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.ContactNameOwner(new gw.api.name.ABPersonNameDelegate(contact as ABPerson)))
    }
    
    // 'def' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 12, column 171
    function def_refreshVariables_98 (def :  pcf.NewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(entity.ABPerson, contact)
    }
    
    // 'value' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 11, column 172
    function defaultSetter_105 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Guardian = (__VALUE_TO_SET as entity.ABPerson)
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at ContactBasicsDV.ABPerson.pcf: line 184, column 62
    function defaultSetter_113 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).FEINOfficialID_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactBasicsDV.ABPerson.pcf: line 189, column 67
    function defaultSetter_117 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).ADANumberOfficialID_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=LicenseNumber_Input) at ContactBasicsDV.ABPerson.pcf: line 194, column 61
    function defaultSetter_121 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).LicenseNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseState_Input) at ContactBasicsDV.ABPerson.pcf: line 201, column 45
    function defaultSetter_125 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).LicenseState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 207, column 50
    function defaultSetter_130 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).LicenseStatus_TDIC = (__VALUE_TO_SET as typekey.GlobalStatus_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=Component_Input) at ContactBasicsDV.ABPerson.pcf: line 213, column 47
    function defaultSetter_134 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Component_TDIC = (__VALUE_TO_SET as typekey.Component_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=CDAMembershipStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 219, column 50
    function defaultSetter_138 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).CDAMembershipStatus_TDIC = (__VALUE_TO_SET as typekey.GlobalStatus_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at ContactBasicsDV.ABPerson.pcf: line 40, column 46
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Credential_TDIC = (__VALUE_TO_SET as typekey.Credential_TDIC)
    }
    
    // 'value' attribute on TextInput (id=Occupation_Input) at ContactBasicsDV.ABPerson.pcf: line 227, column 53
    function defaultSetter_142 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Occupation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 11, column 172
    function defaultSetter_154 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Employer = (__VALUE_TO_SET as entity.ABCompany)
    }
    
    // 'value' attribute on TextAreaInput (id=Notes_Input) at ContactBasicsDV.ABPerson.pcf: line 271, column 46
    function defaultSetter_175 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Notes = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=FormerName_Input) at ContactBasicsDV.ABPerson.pcf: line 47, column 53
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).FormerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=VendorAvailability_Input) at ContactBasicsDV.ABPerson.pcf: line 58, column 34
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.VendorAvailability = (__VALUE_TO_SET as typekey.VendorAvailabilityType)
    }
    
    // 'value' attribute on TextInput (id=VendorUnavailableMessageInput_Input) at ContactBasicsDV.ABPerson.pcf: line 67, column 76
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.VendorUnavailableMessage = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at ContactBasicsDV.ABPerson.pcf: line 127, column 67
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).PreferredCurrency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at ContactBasicsDV.ABPerson.pcf: line 135, column 48
    function defaultSetter_71 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ContactBasicsDV.ABPerson.pcf: line 140, column 40
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.VendorNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=TaxFilingStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 152, column 52
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).TaxFilingStatus = (__VALUE_TO_SET as typekey.TaxFilingStatusType)
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at ContactBasicsDV.ABPerson.pcf: line 157, column 54
    function defaultSetter_86 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).DateOfBirth = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Gender_Input) at ContactBasicsDV.ABPerson.pcf: line 163, column 43
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).Gender = (__VALUE_TO_SET as typekey.GenderType)
    }
    
    // 'value' attribute on TypeKeyInput (id=MaritalStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 169, column 46
    function defaultSetter_94 (__VALUE_TO_SET :  java.lang.Object) : void {
      (contact as ABPerson).MaritalStatus = (__VALUE_TO_SET as typekey.MaritalStatus)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=TaxID_Input) at ContactBasicsDV.ABPerson.pcf: line 135, column 48
    function encryptionExpression_73 (VALUE :  java.lang.String) : java.lang.String {
      return (contact as ABPerson).maskTaxId(VALUE)
    }
    
    // 'filter' attribute on TypeKeyInput (id=LicenseState_Input) at ContactBasicsDV.ABPerson.pcf: line 201, column 45
    function filter_127 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(JurisdictionType.TC_DRIVING_LIC)
    }
    
    // 'initialValue' attribute on Variable at ContactBasicsDV.ABPerson.pcf: line 14, column 23
    function initialValue_0 () : Boolean {
      return contact.CategoryScores != null and contact.CategoryScores.length > 0
    }
    
    // 'initialValue' attribute on Variable at ContactBasicsDV.ABPerson.pcf: line 18, column 49
    function initialValue_1 () : gw.web.ContactDetailsVendorHelper {
      return new gw.web.ContactDetailsVendorHelper(contact)
    }
    
    // 'mode' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 34, column 56
    function mode_12 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'mode' attribute on InputSetRef at ContactBasicsDV.ABPerson.pcf: line 117, column 33
    function mode_58 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'onChange' attribute on PostOnChange at ContactBasicsDV.ABPerson.pcf: line 142, column 80
    function onChange_75 () : void {
      contactDetailsVendorHelper.resetVendorIDFlag(contact) 
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at ContactBasicsDV.ABPerson.pcf: line 40, column 46
    function valueRoot_15 () : java.lang.Object {
      return (contact as ABPerson)
    }
    
    // 'value' attribute on TypeKeyInput (id=VendorAvailability_Input) at ContactBasicsDV.ABPerson.pcf: line 58, column 34
    function valueRoot_28 () : java.lang.Object {
      return contact
    }
    
    // 'value' attribute on TextInput (id=CreateStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 26, column 86
    function valueRoot_4 () : java.lang.Object {
      return contact.CreateStatus
    }
    
    // 'value' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 11, column 172
    function value_104 () : entity.ABPerson {
      return (contact as ABPerson).Guardian
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at ContactBasicsDV.ABPerson.pcf: line 184, column 62
    function value_112 () : java.lang.String {
      return (contact as ABPerson).FEINOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactBasicsDV.ABPerson.pcf: line 189, column 67
    function value_116 () : java.lang.String {
      return (contact as ABPerson).ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=LicenseNumber_Input) at ContactBasicsDV.ABPerson.pcf: line 194, column 61
    function value_120 () : java.lang.String {
      return (contact as ABPerson).LicenseNumber_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseState_Input) at ContactBasicsDV.ABPerson.pcf: line 201, column 45
    function value_124 () : typekey.Jurisdiction {
      return (contact as ABPerson).LicenseState
    }
    
    // 'value' attribute on TypeKeyInput (id=LicenseStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 207, column 50
    function value_129 () : typekey.GlobalStatus_TDIC {
      return (contact as ABPerson).LicenseStatus_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at ContactBasicsDV.ABPerson.pcf: line 40, column 46
    function value_13 () : typekey.Credential_TDIC {
      return (contact as ABPerson).Credential_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Component_Input) at ContactBasicsDV.ABPerson.pcf: line 213, column 47
    function value_133 () : typekey.Component_TDIC {
      return (contact as ABPerson).Component_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=CDAMembershipStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 219, column 50
    function value_137 () : typekey.GlobalStatus_TDIC {
      return (contact as ABPerson).CDAMembershipStatus_TDIC
    }
    
    // 'value' attribute on TextInput (id=Occupation_Input) at ContactBasicsDV.ABPerson.pcf: line 227, column 53
    function value_141 () : java.lang.String {
      return (contact as ABPerson).Occupation
    }
    
    // 'value' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 11, column 172
    function value_153 () : entity.ABCompany {
      return (contact as ABPerson).Employer
    }
    
    // 'value' attribute on TextInput (id=FormerName_Input) at ContactBasicsDV.ABPerson.pcf: line 47, column 53
    function value_17 () : java.lang.String {
      return (contact as ABPerson).FormerName
    }
    
    // 'value' attribute on TextAreaInput (id=Notes_Input) at ContactBasicsDV.ABPerson.pcf: line 271, column 46
    function value_174 () : java.lang.String {
      return (contact as ABPerson).Notes
    }
    
    // 'value' attribute on TypeKeyInput (id=VendorAvailability_Input) at ContactBasicsDV.ABPerson.pcf: line 58, column 34
    function value_26 () : typekey.VendorAvailabilityType {
      return contact.VendorAvailability
    }
    
    // 'value' attribute on TextInput (id=CreateStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 26, column 86
    function value_3 () : java.lang.String {
      return contact.CreateStatus.DisplayName
    }
    
    // 'value' attribute on TextInput (id=VendorUnavailableMessageInput_Input) at ContactBasicsDV.ABPerson.pcf: line 67, column 76
    function value_32 () : java.lang.String {
      return contact.VendorUnavailableMessage
    }
    
    // 'value' attribute on TextInput (id=score_Input) at ContactBasicsDV.ABPerson.pcf: line 81, column 42
    function value_42 () : java.lang.Integer {
      return contact.Score
    }
    
    // 'value' attribute on RowIterator at ContactBasicsDV.ABPerson.pcf: line 93, column 57
    function value_52 () : entity.ABContactCategoryScore[] {
      return contact.getSortedCategoryScores()
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at ContactBasicsDV.ABPerson.pcf: line 127, column 67
    function value_65 () : typekey.Currency {
      return (contact as ABPerson).PreferredCurrency
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at ContactBasicsDV.ABPerson.pcf: line 135, column 48
    function value_70 () : java.lang.String {
      return (contact as ABPerson).TaxID
    }
    
    // 'value' attribute on TextInput (id=vendorNum_Input) at ContactBasicsDV.ABPerson.pcf: line 140, column 40
    function value_76 () : java.lang.String {
      return contact.VendorNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=TaxFilingStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 152, column 52
    function value_81 () : typekey.TaxFilingStatusType {
      return (contact as ABPerson).TaxFilingStatus
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at ContactBasicsDV.ABPerson.pcf: line 157, column 54
    function value_85 () : java.util.Date {
      return (contact as ABPerson).DateOfBirth
    }
    
    // 'value' attribute on TypeKeyInput (id=Gender_Input) at ContactBasicsDV.ABPerson.pcf: line 163, column 43
    function value_89 () : typekey.GenderType {
      return (contact as ABPerson).Gender
    }
    
    // 'value' attribute on TypeKeyInput (id=MaritalStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 169, column 46
    function value_93 () : typekey.MaritalStatus {
      return (contact as ABPerson).MaritalStatus
    }
    
    // 'valueType' attribute on ABContactInput (id=Guardian_Input) at ContactBasicsDV.ABPerson.pcf: line 178, column 40
    function verifyValueType_111 () : void {
      var __valueTypeArg : entity.ABPerson
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.ABContact = __valueTypeArg
    }
    
    // 'valueType' attribute on ABContactInput (id=Organization_Input) at ContactBasicsDV.ABPerson.pcf: line 237, column 41
    function verifyValueType_160 () : void {
      var __valueTypeArg : entity.ABCompany
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.ABContact = __valueTypeArg
    }
    
    // 'visible' attribute on ABContactInput (id=Organization_Input) at ABContactWidget.xml: line 14, column 225
    function visible_148 () : java.lang.Boolean {
      return "ABContactSearchPopup.push(statictypeof ((contact as ABPerson).Employer))" != "" && true
    }
    
    // 'visible' attribute on InputSet at ContactBasicsDV.ABPerson.pcf: line 230, column 67
    function visible_161 () : java.lang.Boolean {
      return isPersonOnly(contact) or isAdjudicator(contact)
    }
    
    // 'visible' attribute on TextInput (id=CreateStatus_Input) at ContactBasicsDV.ABPerson.pcf: line 26, column 86
    function visible_2 () : java.lang.Boolean {
      return contact.CreateStatus != ContactCreationApprovalStatus.TC_APPROVED
    }
    
    // 'visible' attribute on InputSet at ContactBasicsDV.ABPerson.pcf: line 42, column 41
    function visible_21 () : java.lang.Boolean {
      return isPersonOnly(contact)
    }
    
    // 'visible' attribute on TypeKeyInput (id=VendorAvailability_Input) at ContactBasicsDV.ABPerson.pcf: line 58, column 34
    function visible_24 () : java.lang.Boolean {
      return contact.Vendor
    }
    
    // 'visible' attribute on TextInput (id=VendorUnavailableMessageInput_Input) at ContactBasicsDV.ABPerson.pcf: line 67, column 76
    function visible_31 () : java.lang.Boolean {
      return contactDetailsVendorHelper.ShowVendorUnavailableMessage
    }
    
    // 'visible' attribute on InputDivider at ContactBasicsDV.ABPerson.pcf: line 72, column 65
    function visible_39 () : java.lang.Boolean {
      return hasCategoryScores or (contact.Score != null)
    }
    
    // 'visible' attribute on TextInput (id=score_Input) at ContactBasicsDV.ABPerson.pcf: line 81, column 42
    function visible_41 () : java.lang.Boolean {
      return contact.Score != null
    }
    
    // 'visible' attribute on ListViewInput at ContactBasicsDV.ABPerson.pcf: line 84, column 37
    function visible_53 () : java.lang.Boolean {
      return hasCategoryScores
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at ContactBasicsDV.ABPerson.pcf: line 127, column 67
    function visible_64 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on InputSet at ContactBasicsDV.ABPerson.pcf: line 129, column 94
    function visible_80 () : java.lang.Boolean {
      return isPersonOnly(contact) or isAdjudicator(contact) or isPolicyPerson(contact)
    }
    
    // 'visible' attribute on ABContactInput (id=Guardian_Input) at ABContactWidget.xml: line 14, column 225
    function visible_99 () : java.lang.Boolean {
      return "ABContactSearchPopup.push(entity.ABPerson)" != "" && true
    }
    
    property get contact () : ABContact {
      return getRequireValue("contact", 0) as ABContact
    }
    
    property set contact ($arg :  ABContact) {
      setRequireValue("contact", 0, $arg)
    }
    
    property get contactDetailsVendorHelper () : gw.web.ContactDetailsVendorHelper {
      return getVariableValue("contactDetailsVendorHelper", 0) as gw.web.ContactDetailsVendorHelper
    }
    
    property set contactDetailsVendorHelper ($arg :  gw.web.ContactDetailsVendorHelper) {
      setVariableValue("contactDetailsVendorHelper", 0, $arg)
    }
    
    property get hasCategoryScores () : Boolean {
      return getVariableValue("hasCategoryScores", 0) as Boolean
    }
    
    property set hasCategoryScores ($arg :  Boolean) {
      setVariableValue("hasCategoryScores", 0, $arg)
    }
    
    function isVendor(aContact : ABContact) : boolean {
          return aContact typeis ABPersonVendor;
          }
    
          function isAdjudicator(aContact : ABContact) : boolean {
          return aContact typeis ABAdjudicator;
          }
    
          function isPersonOnly(aContact : ABContact) : boolean {
          return aContact.Subtype==TC_ABPERSON;
          }
    
          function isPolicyPerson(aContact : ABContact) : boolean {
          return aContact typeis ABPolicyPerson;
          }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contacts/basics/ContactBasicsDV.ABPerson.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ContactBasicsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Category_Cell) at ContactBasicsDV.ABPerson.pcf: line 100, column 53
    function valueRoot_47 () : java.lang.Object {
      return categoryScore
    }
    
    // 'value' attribute on TypeKeyCell (id=Category_Cell) at ContactBasicsDV.ABPerson.pcf: line 100, column 53
    function value_46 () : typekey.ReviewCategory {
      return categoryScore.ReviewCategory
    }
    
    // 'value' attribute on TextCell (id=Score_Cell) at ContactBasicsDV.ABPerson.pcf: line 106, column 48
    function value_49 () : java.lang.Integer {
      return categoryScore.Score
    }
    
    property get categoryScore () : entity.ABContactCategoryScore {
      return getIteratedValue(1) as entity.ABContactCategoryScore
    }
    
    
  }
  
  
}