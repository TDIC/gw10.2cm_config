package gw.api.phone.upgrade

uses gw.api.system.PLLoggerCategory
uses gw.api.util.PhoneUtil
uses gw.api.util.phone.GWPhoneNumber
uses gw.plugin.Plugins
uses gw.plugin.phone.IPhoneNormalizerPlugin

/**
 * File created by Guidewire Upgrade Tools.
 */
class PhoneParser {

  private static var logger = PLLoggerCategory.GLOBALIZATION_PHONE_NORMALIZER

  /**
   * Uses the GW OOTB plugin to parse the phone number. In case the phone number passed in this
   * function is invalid, it will still build the GWPhoneNumber object and store the whole number in the
   * main phone field
   *
   * @param number the phone number to parse
   * @return the phone number split in the 3 phone fields
   */
  static function exParsePhone(number: String): GWPhoneNumber {
    if (not number.NotBlank) {
      return null
    }
    var gwPhoneNumber = Plugins.get(IPhoneNormalizerPlugin).parsePhoneNumber(number)
    if (gwPhoneNumber == null) {
      logger.error("Phone number " + number + " could not be parsed. The whole number will be stored in the main phone field")
      gwPhoneNumber = PhoneUtil.buildPhoneNumbers(null, number, null)
    } else if (gwPhoneNumber.CountryCode == PhoneCountryCode.TC_UNPARSEABLE) {
      logger.warn("Phone number " + number + " is not a possible number")
    }
    return gwPhoneNumber
  }
}