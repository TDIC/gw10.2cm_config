package gw.scriptparameter

uses java.math.BigDecimal
/**
* File created by Guidewire Upgrade Tools.
*/
@Export
enhancement Ex_ScriptParametersEnhancement : ScriptParameters {

    public static property get LawsonRequestThreshold(): Integer {
        return ScriptParameters.getParameterValue("LawsonRequestThreshold") as Integer
    }

}