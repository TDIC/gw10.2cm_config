package tdic.ab.common.batch.vendornumber.dto

/**
 * USUS556
 * 04/08/2015 Kesava Tavva
 *
 * Object used to store fields for vendor number records retrieved from Lawson.
 */
class TDIC_VendorRecord {
  var _taxID : String as TaxID
  var _vendorNumber : String as  VendorNumber
  var _comments : String as Comments

  /**
   * US
   * 04/08/2015 Kesava Tavva
   *
   * Override function to return field values in a String object.
   */
  override function toString() : String{
    return "${TaxID} | ${VendorNumber} | ${Comments}"
  }
}