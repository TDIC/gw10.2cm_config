package tdic.ab.integ.services.lawson.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 * Sprint - 5, GINTEG - 21 : CM Lawson Contact API
 * Below DTO contains details of the contact for which the Address is Changed
 * Author: Sudheendra
 * Date: 09/14/2019
 */
@WsiExportable
final class TDIC_ContactsWithAddrsChangeDTO {

  var _vendor_vname : String as VENDOR_VNAME
  var _taxid : String as TAX_ID
  var _legal_name : String as LEGAL_NAME
  var _vendor : String as VENDOR
  var _vendor_group : String as VENDOR_GROUP
  var _old_vendor : String as OLD_VENDOR
  var _location_code : String as LOCATION_CODE
  var _effective_date : String as EFFECTIVE_DATE
  var _addr1 : String as ADDR1
  var _addr2 : String as ADDR2
  var _addr3 : String as ADDR3
  var _addr4 : String as ADDR4
  var _city_addr5 : String as CITY_ADDR5
  var _state_prov : String as STATE_PROV
  var _postal_code : String as POSTAL_CODE
  var _county : String as COUNTY
  var _country_code : String as COUNTRY_CODE
  var _region : String as REGION
  var _resp_code : String as RESP_CODE
  var _contact_lvl : String as CONTACT_LVL

}