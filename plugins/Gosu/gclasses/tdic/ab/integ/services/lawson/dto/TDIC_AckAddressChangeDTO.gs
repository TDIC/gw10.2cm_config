package tdic.ab.integ.services.lawson.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 * Sprint - 5, GINTEG - 21 : CM Lawson Contact API
 * Below DTO will contain Vendor Numbers for which Lawson has acknowledged Address Change
 * Author: Sudheendra
 * Date: 09/14/2019
 */
@WsiExportable
final class TDIC_AckAddressChangeDTO {

  var _vendor : String as VENDOR

}