package tdic.ab.integ.plugins.lawson

uses gw.plugin.messaging.MessageRequest
uses gw.api.system.PLLoggerCategory
uses tdic.ab.integ.plugins.lawson.helper.TDIC_LawsonHelper
uses gw.api.database.Query
uses org.apache.commons.lang.StringUtils
uses java.lang.Exception

/**
 * US556 - Lawson Contact
 * 07/07/2015 Kesava Tavva
 *
 * Implementation of a Messaging Request plugin to process Lawson Contact.  This is mainly for setting the
 * SenderRefID field and update vendor number if payload doesn't have it.
 */
class TDIC_LawsonContactMessageRequest implements MessageRequest {

  /**
   * Standard logger.
   */
// Sudheendra : Logger for Lawson Contact Update
  private static var _logger = PLLoggerCategory.TDIC_LAWSONCONTACTUPD

  /**
   * Sets the SenderRefID on the message.
   */
  @Param("msg","the original message")
  @Returns("A String for the payload")
  override function beforeSend(msg: entity.Message): String {
    _logger.debug("TDIC_LawsonContactMessageRequest#beforeSend() - Entering")
    if (msg.SenderRefID == null) {
      msg.SenderRefID = msg.PublicID
    }
    _logger.debug("TDIC_LawsonContactMessageRequest#beforeSend() - SenderRefID: " + msg.SenderRefID)
    if (msg.EventName == "ABContactChanged" && msg.Payload.contains(TDIC_LawsonHelper.VENDOR_NUMBER_NOT_AVAILABLE)) {
      _logger.debug("TDIC_LawsonContactMessageRequest#beforeSend() - Payload: " + msg.Payload)
      try{
        var splitPayload = msg.Payload?.split('#')
        if(splitPayload?[1].HasContent){
          _logger.debug("TDIC_LawsonContactMessageRequest#beforeSend() - Contact PublicID: " + splitPayload[1])
          var aContact = Query.make(ABContact).compare(ABContact#PublicID,Equals,splitPayload[1]).select().AtMostOneRow
          if(aContact.VendorNumber.HasContent){
            _logger.info("TDIC_LawsonContactMessageRequest#beforeSend() - VendorNumber found for Contact PublicID: ${splitPayload[1]} is ${aContact.VendorNumber}")
            msg.Payload = splitPayload[0].replaceAll(TDIC_LawsonHelper.VENDOR_NUMBER_NOT_AVAILABLE,"")+StringUtils.leftPad(aContact.VendorNumber,9," ")+splitPayload[2]
            _logger.debug("TDIC_LawsonContactMessageRequest#beforeSend() - Payload updated with vendor number: " + msg.Payload)
          }
        }
      }catch(e: Exception){
        _logger.error("TDIC_LawsonContactMessageRequest#beforeSend() - Error: ${e.Message} in processing payload: ${msg.Payload}")
      }
    }
    return msg.Payload
  }

  override function afterSend(p0: entity.Message) {
  }

  override function shutdown() {
  }

  override function suspend() {
  }

  override function resume() {
  }

  property set DestinationID(p0: int) {
  }
}