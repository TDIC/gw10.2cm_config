package tdic.ab.config.enhancements
/**
 * Created with IntelliJ IDEA.
 * User: KunalB
 * Date: 6/6/16
 * Time: 2:16 PM
 *
 * TDIC ABContact Enhancement
 */
enhancement TDIC_ABContactEnhancement : entity.ABContact {
  property get ADANumberOfficialID_TDIC() : String {
    var adaNumberOfficialID = this.OfficialIDs.firstWhere( \ id -> id.OfficialIDType == typekey.OfficialIDType.TC_ADANUMBER_TDIC)
    return adaNumberOfficialID.OfficialIDValue
  }

  property set ADANumberOfficialID_TDIC(adaNumber : String) {
    var adaNumberOfficialID = this.OfficialIDs.firstWhere( \ id -> id.OfficialIDType == typekey.OfficialIDType.TC_ADANUMBER_TDIC)
    if (adaNumberOfficialID == null) {
      adaNumberOfficialID = new ABOfficialID()
      adaNumberOfficialID.OfficialIDType = typekey.OfficialIDType.TC_ADANUMBER_TDIC
      this.addToOfficialIDs(adaNumberOfficialID)
    }
    adaNumberOfficialID.OfficialIDValue = adaNumber
  }
}
