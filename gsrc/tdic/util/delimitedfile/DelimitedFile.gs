package tdic.util.delimitedfile

uses gw.pl.logging.LoggerCategory
uses gw.xml.XmlElement
uses org.apache.commons.lang.StringUtils
uses org.apache.poi.xssf.usermodel.XSSFCell
uses org.apache.poi.xssf.usermodel.XSSFRow
uses org.apache.poi.xssf.usermodel.XSSFWorkbook
uses java.io.File
uses java.io.FileInputStream
uses java.io.PrintWriter
uses java.lang.Exception
uses java.text.SimpleDateFormat
uses java.util.ArrayList
uses java.util.Calendar
uses java.util.HashMap
uses java.util.Map
uses tdic.util.cache.CacheManager
uses gw.pl.exception.GWConfigurationException

/**
 * Abstract class to process flat files.  This class must be extended to specify items such as the vendor file name,
 * header and trailer fields, and the way to process non-XML fields.
 */
abstract class DelimitedFile {

  private static var _logger = LoggerCategory.TDIC_INTEGRATION

  /**
   * Key for retrieving the relative path for vendor spec Excel spreadsheet directory from the integration database
   * (value: VendorSpecRelativePath).
   */
  public static final var VENDOR_SPEC_PATH_KEY : String = "VendorSpecRelativePath"

  /**
   * Encodes lines of the flat file based on the vendor spec Excel spreadsheet.
   * Only the file name is passed in since the relative path is already determined.
   */
  @Param("vendorSpecFileName", "A String specifying the file name of the vendor spec Excel spreadsheet")
  @Param("xml", "An XmlElement containing the data to encode")
  @Returns("A List<String> of the encoded lines to be written to the flat file")
  @Throws(Exception, "If unexpected exception is caught and rethrown while encoding")
  public function encode(vendorSpecFileName : String, xml: XmlElement): List<String> {
    _logger.info("DelimitedFile#encode() - Entering")
    var _outputLines : List<String>
    try {
      var _fieldSpecMap = getFieldSpec(vendorSpecFileName)
      if (_logger.DebugEnabled) {
        _logger.debug("FIELD SPEC MAP:")
        for (key in _fieldSpecMap.Keys) {
          _logger.debug("Record Type: " + key)
          for (spec in _fieldSpecMap.get(key)) {
            _logger.debug("  Spec: " + spec)
          }
        }
      }
      _outputLines = new ArrayList<String>()
      for (headerRecordType in HeaderRecordTypes) {
        headerRecordType = headerRecordType.toUpperCase()
        _logger.debug("Attempting to get header record type: " + headerRecordType)
        var headerFieldSpec = _fieldSpecMap.get(headerRecordType)
        if (headerFieldSpec != null) {
          _logger.debug("Found header record type: " + headerRecordType)
          _outputLines.add(encodeRecord(xml,headerFieldSpec))
        }
        else {
          _logger.warn("Unable to find header record type: " + headerRecordType)
        }
      }
      for(child in xml.$Children){
        _logger.debug("Processing Started for Child LocalPart: ${child.$QName.LocalPart} --->${child}; ")
        var recordFieldSpecList = _fieldSpecMap.get(child.$QName.LocalPart.trim()?.toUpperCase())
        _logger.debug("RecordType: ${child.$QName.LocalPart.toUpperCase()} | RecordFields: ${recordFieldSpecList}")
        if(not recordFieldSpecList.HasElements){
          _logger.warn("Fields not found for Children : ${child.$QName.LocalPart}")
          _logger.debug("Processing Completed for Children--->${child}")
          continue
        }
        // Handling of arrays within GX Model
        if (child.$Children.allMatch( \ entryChild -> entryChild.$QName.LocalPart == "Entry")) {
          for (entryChild in child.$Children) {
            _logger.debug("Processing Array within Child: ${child.$QName.LocalPart}")
            _outputLines.add(encodeRecord(entryChild, recordFieldSpecList))
          }
        }
        else {
          _outputLines.add(encodeRecord(child, recordFieldSpecList))
        }
        _logger.debug("Processing Completed for Children--->${child}")
      }
      for (trailerRecordType in TrailerRecordTypes) {
        trailerRecordType = trailerRecordType.toUpperCase()
        _logger.debug("Attempting to get trailer record type: " + trailerRecordType)
        var trailerFieldSpec = _fieldSpecMap.get(trailerRecordType)
        if (trailerFieldSpec != null) {
          _logger.debug("Found trailer record type: " + trailerRecordType)
          _outputLines.add(encodeRecord(xml,trailerFieldSpec))
        }
        else {
          _logger.warn("Unable to find header record type: " + trailerRecordType)
        }
      }
      _logger.info("DelimitedFile#encode() - Exiting")
      return _outputLines
    } catch (e : Exception) {
      _logger.error("DelimitedFile#encode() - Error while encoding: " + e.LocalizedMessage, e)
      throw e
    }
  }

  /**
   * Reads the field specs from the vendor spec Excel spreadsheet.
   * Only the file name is passed in since the relative path is already determined.
   */
  @Param("vendorSpecFileName", "A String specifying the file name of the vendor spec Excel spreadsheet")
  @Returns("A Map<List<FieldSpec>> mapping the record type string to the list of FieldSpecs (each row for each individual field)")
  @Throws(Exception, "If an unexpected Exception is caught while reading the spreadsheet")
  private function getFieldSpec(vendorSpecFileName : String): Map<String, List<FieldSpec>> {
    var vendorSpecDir = CacheManager.PropertiesCache.get(VENDOR_SPEC_PATH_KEY)
      if (vendorSpecDir == null) {
      throw new GWConfigurationException("Cannot retrieve vendor spec path with the key '" + VENDOR_SPEC_PATH_KEY
          + "' from integration database.")
    }

    var _inputFile = new File(vendorSpecDir + vendorSpecFileName)
    _logger.info("Reading vendor spec file: " + _inputFile.AbsolutePath)
    var _vendorSpecSpreadsheet = new XSSFWorkbook(new FileInputStream(_inputFile))
    var _recordsSpec = new HashMap<String, List<FieldSpec>>()

    try {
      // Get the 2nd sheet in the spreadsheet containing the records specifications
      var _sheet = _vendorSpecSpreadsheet.getSheet("Fields")
      var _rowsCount = _sheet.getPhysicalNumberOfRows() - 1      // not counting the header

      var _row: XSSFRow
      var _cell: XSSFCell
      var _cells: List<String>
      var _fields = new ArrayList<FieldSpec>()

      var _previousRecType = ""
      var _recordType: String

      for (r in 1.._rowsCount) {
        _row = _sheet.getRow(r)
        if (_row != null) {
          _recordType = _row.getCell(0)?.StringCellValue

          // Check to be sure it is not the last row, since a blank line at the end of the file would skip the entire last record type due to the "continue" statement above
          if (not _recordType.HasContent && r!=_rowsCount) {
            continue
          }

          _cells = new ArrayList<String>()
          for (0..FieldSpec.COLUMN_COUNT index c) {
            _cell = _row.getCell(c + 1)
            if (_cell != null) {
              _cells.add(c, _cell as String)
            }
          }

          if(_previousRecType.HasContent && not _recordType.equalsIgnoreCase(_previousRecType)) {
            _recordsSpec.put(_previousRecType?.trim().toUpperCase(), _fields)
            _fields = new ArrayList<FieldSpec>()
            var list = _recordsSpec.get(_previousRecType?.trim().toUpperCase())
          }
          _fields.add(new FieldSpec(_cells))
          _previousRecType = _recordType

          // Check to be sure last row has content before adding as a new record type
          if(_recordType.HasContent && r == _rowsCount) {
            _recordsSpec.put(_recordType?.trim().toUpperCase(), _fields)
          }
        }
      }
    } catch (var e : Exception) {
      _logger.error("DelimitedFile#getFieldSpec() - Exception while parsing spreadsheet: " + e.LocalizedMessage, e)
      throw e
    }
    return _recordsSpec
  }

  /**
   * Encodes a single line either based on fixed data / non-XML data, or based on the data inside an XmlElement.
   */
  @Param("xml", "The XmlElement containing the data to encode")
  @Param("fieldSpecList", "The List<FieldSpec> containing the collection of FieldSpecs for the specific record type")
  @Returns("A String containing the encoded line for the field")
  @Throws(Exception, "If unexpected exception is caught and rethrown while encoding")
  private function encodeRecord(xml : XmlElement, fieldSpecList: List<FieldSpec>): String {
    var line = ""
    try {
      for (fieldSpec in fieldSpecList){
        var value : String
        // Specify type of non-XML field
        if (fieldSpec.Field.startsWithIgnoreCase("Fixed.")) {
          value = getNonXMLFieldValue(fieldSpec)
        }
        else {
          value = getFieldValue(xml, StringUtils.substring(fieldSpec.Field, fieldSpec.Field.indexOf(".")+1, fieldSpec.Field.size))   //Get value from Xml
        }
        if (value == null) {
          _logger.warn("Unable to get the value of the field '" + fieldSpec.Field + "' based on the FieldSpec")
          _logger.warn("XML is")
          _logger.warn(xml.asUTFString())
        }
        line += formatString(value, fieldSpec.Start, fieldSpec.Length.toInt(), fieldSpec.Truncate,
            fieldSpec.Justify, fieldSpec.Fill, fieldSpec.Format, fieldSpec.Strip, fieldSpec.Separator) //Format string and add it to flat file line
      }
      _logger.debug("DelimitedFile#encodeRecord() - Flat line is: --->" + line)

    } catch (var e:Exception){
      _logger.error("DelimitedFile#encodeRecord() - Unexpected Exception while encoding record: " + e.LocalizedMessage, e)
      throw e
    }
    return line
  }

  /**
   * Gets the value for a field based on the data inside an XmlElement.
   */
  @Param("anElement", "The XmlElement containing the data to encode")
  @Param("compositeTag", "A String containing the name of the field whose data to retrieve from the XmlElement")
  @Returns("A String containing the value for the field")
  @Throws(Exception, "If unexpected exception is caught while getting the value from the XML data")
  private function getFieldValue(anElement: XmlElement, compositeTag: String): String {
    // Split the tag at the dot into a String[]
    var _tags = compositeTag.split("\\.")
    for(_tag in _tags){
      try {
        anElement = anElement?.$Children.firstWhere( \ elt -> elt.$QName.LocalPart.equalsIgnoreCase(_tag))
      } catch(var e : Exception){
        _logger.error("DelimitedFile#getFieldValue() - Unable to get the value of the field based on the FieldSpec. Exception: " + e.LocalizedMessage, e)
        throw e
      }
    }
    return anElement.$Text
  }

  /**
   * Applies additional formatting to the field based on the FieldSpec values.
   */
  @Param("value", "The field value on which additional formatting is being applied")
  @Param("start", "The starting position of the field, which if blank, indicates that it is not a fixed width field and therefore does not need padding")
  @Param("destLength", "An int specifies the maximum length of the value")
  @Param("truncate", "A String specifying which direction to truncate the data, if necessary")
  @Param("justify", "A String specifying which direction to justify the data")
  @Param("file", "A String specifying which character to fill the empty spaces in the data")
  @Param("format", "A String specifying which the format which the data must follow")
  @Param("strip", "A String specifying the character(s) to remove from the value")
  @Param("separator", "A String specifying the character(s) to use as the separator after the value")
  @Returns("The formatted String")
  private function formatString(value:String, start:String, destLength:int, truncate:String, justify:String, fill:String, format:String, strip:String, separator:String):String{
    //First strip unnecessary characters
    if(strip != "'"){   //Default value is "'". So, remove chars if it is not the default value
      value=StringUtils.remove(value,strip) //Strip the values
    }
    if(fill=="'"){        //Default value of fill is "'" which is equal to a white space
      fill = " "
    }
    if(value==null){     //Handle null values
      value=fill
    }
    if(format=="S9(9)v99"){// Financial fields has a special requirement. Last two digits are considered as decimals. $ is replaced by +
      var currencyArray = value.split("\\.")                                   //Split at dot.
      if(currencyArray.length==2){                                             //Valid currency
        value = currencyArray[0]+StringUtils.rightPad(currencyArray[1],0,"2")  //Right pad decimals with 0 and append currency value to it
        value = StringUtils.leftPad(value,destLength-1,"0")                    //Left pad value to destLength-1 with zeros
        value = "+"+value                                                      //Append + sign
      }
    }
    else {
      if(format[0]=="9"){              //If value is an integer, discard decimals
        value = value.split("\\.")[0]
        fill = fill.split("\\.")[0]
      }}
    var newValue = value
    if(!start.Empty && start != "'" && value.length < destLength ) {  //String length is less than required length. Do padding
      if (justify=="Right"){              //Right pad with fill values
        newValue = StringUtils.leftPad(value,destLength,fill)
      }
      else if (justify=="Left"){          //Left pad with fill values
        newValue = StringUtils.rightPad(value,destLength,fill)
      }
    }
    else if(value.length > destLength ) {  //String length is greater than required length. Truncate
      if(truncate == "Right"){           //Truncate right
        newValue = value.substring(0,destLength)
      }
      else if(truncate == "Left"){       //Truncate Left
        newValue = value.substring(value.length-destLength,value.length)
      }
    }
    // Add separator if needed
    if (separator != "'") {
      newValue = newValue + separator
    }
    return newValue
  }

  /**
   * Writes lines to a flat file.
   */
  @Param("lines", "A List<String> containing the lines to write to the flat file")
  @Param("filePath", "A String containing the path of the destination flat file")
  @Param("fileName", "A String containing the file name of the destination flat file")
  @Throws(Exception, "If there is an unexpected exception writing to the flat file")
  protected function writeToFile(lines : List<String>, filePath : String, fileName : String) {
    if(!new File(filePath).exists()){
      throw new Exception("Cannot Access ${filePath}. The Directory May Not Exist or There is an Error in network Connection")
    } else {
      _logger.info(" Access to ${filePath} is Successful.")
    }
    var flatFile:PrintWriter
    try {
      var fileSuffix = new SimpleDateFormat("MM-dd-yyyy-HHmmss").format(Calendar.getInstance().getTime())
      flatFile = new PrintWriter("${filePath}${fileName}")
      lines?.each( \ line -> {
        flatFile.println(line)
      })
      flatFile.close()
    } catch(var e:Exception){
      _logger.error("DelimitedFile#writeToFile() - Exception occurred while writing lines to a file ${filePath}: " + e.LocalizedMessage ,e)
      throw new Exception("Exception occurred while writing Premium lines to a file ${filePath}", e)
    }
  }

  /**
   * Abstract function to be implemented.  This must determine the behavior of reading non-XML / fixed fields.
   */
  @Param("fieldSpec", "The FieldSpec object containing the field name and other fields necessary to encode the non-XML field")
  @Returns("A String containing the value of the non-XML field")
  abstract function getNonXMLFieldValue(fieldSpec : FieldSpec) : String

  /**
   * Abstract function to be implemented to specify the record types of the header lines.
   * Note: Implement with a blank list if there are no header lines.
   */
  @Returns("A List<String> containing the record types for the header lines")
  abstract property get HeaderRecordTypes() : List<String>

  /**
   * Abstract function to be implemented to specify the record types of the trailer lines.
   * Note: Implement with a blank list if there are no trailer lines.
   */
  @Returns("A List<String> containing the record types for the trailer lines")
  abstract property get TrailerRecordTypes() : List<String>

}