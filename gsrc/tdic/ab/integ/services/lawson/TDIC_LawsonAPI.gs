package tdic.ab.integ.services.lawson

uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.logging.LoggerCategory
uses gw.xml.ws.annotation.WsiWebService
uses tdic.ab.integ.plugins.lawson.helper.TDIC_LawsonHelper
uses tdic.ab.integ.services.lawson.dto.*
uses javax.xml.soap.SOAPException
uses java.text.SimpleDateFormat

/**
 * Sprint-5,GINTEG-21 : This WebService is created to server below purpose.
 * Lawson-AP needs to know about newly created Primary Named Insured contacts in XCenter and it generate a vendor number.
 * It also needs to know about updates to Contacts addresses made in PC. PC need to know about newly given vendor numbers in Lawson-AP.
 * Author: Sudheendra
 * Date: 09/14/2019
 */

@WsiWebService("http://guidewire.com/ab/ws/tdic/ab/integ/services/lawson/TDIC_LawsonAPI")
@Export
class TDIC_LawsonAPI {

  static final var _truncLength = 30
// Sudheendra : Logger for Lawson Contact Update
  static final var _logger = LoggerCategory.TDIC_LAWSONCONTACTUPD
  static final var vendorGroup = "VMST"
  static final var locationCode_BC = "80"
  static final var locationCode_CC = "70"
  static final var locationType = "B"
  static final var venClass = "NR"
  static final var termCode = "NET0"
  static final var regEx = "[^0-9 A-Z a-z]"
  static final var vendorNumFormat = "%9s"
  static final var dateFormat = "yyyyMMdd"
  static final var paymentCatCode_BC = "RFW"
  static final var paymentCatCode_CC = "GCL"
  static final var cashCode = "0510"
  public static final var VENDOR_NUMBER_NOT_AVAILABLE : String = "VendorNumber not available for Contact"

  @Throws(SOAPException, "If communication errors occur")
  @Returns("List of contacts which is of type primary named insured which is newly added in ContactManager")
  function getNewlyAddedContacts() : List<TDIC_LawsonContactDTO> {
    _logger.debug("Started Executing 'getNewlyAddedContacts()' method of TDIC_LawsonAPI")
    // return all the contacts with indicator 'NeedVendorID' set to 'true'
    var needVendorID = Query.make(entity.ABContact).compare(ABContact#NeedVendorID, Equals, true).compare(ABContact#TaxID, NotEquals, null)
        .join("PrimaryAddress").compare(Address#AddressLine1, NotEquals , null).compare(Address#City, NotEquals, null)
        .compare(Address#State, NotEquals, null).compare(Address#PostalCode, NotEquals, null).select().toList()
    var needVendorIDList = new ArrayList<ABContact>(){}
    var threholdCount = ScriptParameters.getParameterValue("LawsonRequestThreshold") as int
    if(needVendorID.Count >  threholdCount){
      for (elemIndex in 0..|threholdCount) {
        needVendorIDList.add(needVendorID[elemIndex])
      }
      needVendorID = needVendorIDList
    }
    var lawsonHelper = new TDIC_LawsonHelper()
    var listOfContNeedAddrID = new ArrayList<TDIC_LawsonContactDTO>()
    needVendorID.each(\abContact -> {
      var lawsonContactDTO = new TDIC_LawsonContactDTO()
      listOfContNeedAddrID.add(lawsonContactDTO)
      //GINTEG-686 : Corrected code which was sending Last Name as ADDR1
      var formattedLines : String[]
      var vendor_vname = ""
      if(abContact typeis ABCompany){
        vendor_vname = (lawsonHelper.limitLength(lawsonHelper.fillNull(abContact.Name).replaceAll("[^A-Z a-z 0-9]", ""), 30)).toUpperCase()
        formattedLines = lawsonHelper.formatLines(_truncLength, 5, {lawsonHelper.fillNull(abContact.PrimaryAddress?.AddressLine1),
            lawsonHelper.fillNull(abContact.PrimaryAddress?.AddressLine2),
            lawsonHelper.fillNull(abContact.PrimaryAddress?.AddressLine3)})

      } else {
        var person = abContact as ABPerson
        vendor_vname = "${lawsonHelper.fillNull(person.FirstName)} ${lawsonHelper.fillNull(person.MiddleName == null ? "" : person.MiddleName.charAt(0) as String)} ${lawsonHelper.fillNull(person.LastName)} ${lawsonHelper.fillNull(person.Suffix as String)} ${lawsonHelper.fillNull(person.Credential_TDIC as String)}"
        vendor_vname = (lawsonHelper.limitLength(vendor_vname.replaceAll("[^A-Z a-z 0-9]", ""), 30)).toUpperCase()
        formattedLines = lawsonHelper.formatLines(_truncLength, 5, {lawsonHelper.fillNull(person.PrimaryAddress?.AddressLine1),
            lawsonHelper.fillNull(person.PrimaryAddress?.AddressLine2),
            lawsonHelper.fillNull(person.PrimaryAddress?.AddressLine3)})
      }
      var isBCContact = abContact.TagTypes.length == 1 && abContact.TagTypes.first() == ContactTagType.TC_CLIENT
      lawsonContactDTO.VENDOR_GROUP = lawsonHelper.fillNull(vendorGroup) //VENDOR_GROUP
      lawsonContactDTO.VENDOR = '' //VENDOR
      lawsonContactDTO.OLD_VENDOR = abContact.PublicID //OLD_VENDOR
      lawsonContactDTO.LOCATION_CODE = lawsonHelper.fillNull(isBCContact ? locationCode_BC : locationCode_CC)  //LOCATION_CODE
      lawsonContactDTO.LOC_TYPE = lawsonHelper.fillNull(locationType) //LOCATION_TYPE
      lawsonContactDTO.VEN_CLASS = lawsonHelper.fillNull(venClass) //VEN_CLASS
      lawsonContactDTO.VENDOR_VNAME = vendor_vname //VENDOR_VNAME
      lawsonContactDTO.VENDOR_SNAME = '' //VENDOR_SNAME
      lawsonContactDTO.VENDOR_CONTCT = '' //VENDOR_CONTCT
      lawsonContactDTO.ADDR1 = (lawsonHelper.limitLength(lawsonHelper.fillNull(formattedLines[0]), 30)).toUpperCase() //ADDR1
      lawsonContactDTO.ADDR2 = (lawsonHelper.limitLength(lawsonHelper.fillNull(formattedLines[1]), 30)).toUpperCase() //ADDR2
      lawsonContactDTO.ADDR3 = (lawsonHelper.limitLength(lawsonHelper.fillNull(formattedLines[2]), 30)).toUpperCase() // ADDR3
      lawsonContactDTO.ADDR4 = (lawsonHelper.limitLength(lawsonHelper.fillNull(formattedLines[3]), 30)).toUpperCase()  //ADDR4
      lawsonContactDTO.CITY_ADDR5 = (lawsonHelper.limitLength(lawsonHelper.fillNull(abContact.PrimaryAddress.City), 18)).toUpperCase() //CITY_ADDR5
      lawsonContactDTO.STATE_PROV = lawsonHelper.fillNull(abContact.PrimaryAddress.State.Code).toUpperCase() //STATE_PROV
      lawsonContactDTO.POSTAL_CODE = lawsonHelper.fillNull(abContact.PrimaryAddress.PostalCode).toUpperCase() //POSTAL_CODE
      //GINTEG-1176 - Comment out county and country
      lawsonContactDTO.COUNTY = '' //(lawsonHelper.limitLength(lawsonHelper.fillNull(abContact.PrimaryAddress.County), 25)).toUpperCase()  //COUNTY
      lawsonContactDTO.COUNTRY = '' //lawsonHelper.fillNull(abContact.PrimaryAddress.Country.Code).toUpperCase()  //COUNTRY
      lawsonContactDTO.REGION = ''  //REGION
      lawsonContactDTO.COUNTRY_CODE = '' //COUNTRY_CODE
      lawsonContactDTO.NORM_REMIT = ''  //NORM_REMIT
      lawsonContactDTO.NORM_PURCH = '' //NORM_PURCH
      lawsonContactDTO.REMIT_TO_CODE = '' //REMIT_TO_CODE
      lawsonContactDTO.PURCH_FR_LOC = '' //PURCH_FR_LOC
      lawsonContactDTO.PAY_VENDOR = '' //PAY_VENDOR
      lawsonContactDTO.VENDOR_STATUS = ''  //VENDOR_STATUS
      lawsonContactDTO.VEN_PRIORITY = 0 //VEN_PRIORITY
      lawsonContactDTO.PHONE_PREFIX = ''//PHONE_PREFIX
      lawsonContactDTO.PHONE_NUM = ''//PHONE_NUM
      lawsonContactDTO.PHONE_EXT = ''//PHONE_EXT
      lawsonContactDTO.FAX_PREFIX = '' //FAX_PREFIX
      lawsonContactDTO.FAX_NUM = '' //FAX_NUM
      lawsonContactDTO.FAX_EXT = '' //FAX_EXT
      lawsonContactDTO.TELEX_NUM = '' //TELEX_NUM
      lawsonContactDTO.TERM_CODE = termCode //TERM_CODE
      lawsonContactDTO.INV_CURRENCY = '' //INV_CURRENCY
      lawsonContactDTO.BAL_CURRENCY = '' //BAL_CURRENCY
      lawsonContactDTO.BANK_CURRENCY = '' //BANK_CURRENCY
      lawsonContactDTO.CURR_RECALC = '' //CURR_RECALC
      lawsonContactDTO.SEP_CHK_FLAG = '' //SEP_CHK_FLAG
      lawsonContactDTO.TAX_ID = lawsonHelper.fillNull(abContact.TaxID) //TAX_ID
      lawsonContactDTO.TAX_CODE = '' //TAX_CODE
      lawsonContactDTO.HLD_CODE = '' //HLD_CODE
      lawsonContactDTO.DIST_CODE = '' //DIST_CODE
      lawsonContactDTO.ACCR_CODE = ''//ACCR_CODE
      lawsonContactDTO.BANK_INST_CODE = '' //BANK_INST_CODE
      lawsonContactDTO.CASH_CODE = cashCode //CASH_CODE
      lawsonContactDTO.BANK_ENTITY = ''//BANK_ENTITY
      lawsonContactDTO.VBANK_ACCT_NO = '' //VBANK_ACCT_NO
      lawsonContactDTO.VBANK_IDENT = '' //VBANK_IDENT
      lawsonContactDTO.RIBKEY = 0 //RIBKEY
      lawsonContactDTO.VBANK_ACCT_TP = '' //VBANK_ACCT_TP
      lawsonContactDTO.INCOME_CODE = '' //INCOME_CODE
      lawsonContactDTO.INCOME_WH_FLG = '' //INCOME_WH_FLG
      lawsonContactDTO.EDI_NBR = '' //EDI_NBR
      lawsonContactDTO.ACH_PRENOT = '' //ACH_PRENOT
      lawsonContactDTO.MAX_INV_AMT = 0 //MAX_INV_AMT
      lawsonContactDTO.ORIGIN_DATE = lawsonHelper.fillNull(DateUtil.currentDate().toSQLDate().toString())  //ORIGIN_DATE
      lawsonContactDTO.PublicID = lawsonHelper.fillNull(abContact.PublicID) //USER_NAME_01 = PublicID
      //lawsonContactDTO.TAX_ID = '' //USER_NAME_02 = TaxID (FEIN/SSN)
      //lawsonContactDTO.USER_NAME_02 = ''
      lawsonContactDTO.PersonFEIN_TDIC = ''//USER_NAME_03 = PersonFEIN_TDIC
      lawsonContactDTO.SSN = '' //USER_NAME_04 = SSN
      lawsonContactDTO.STax = '' //USER_NAME_05 = STax
      lawsonContactDTO.BureauID = '' //USER_NAME_06 = BureauID
      lawsonContactDTO.CUST_GROUP = '' //CUST_GROUP
      lawsonContactDTO.CUSTOMER = '' //CUSTOMER
      lawsonContactDTO.LEGAL_NAME = (lawsonHelper.limitLength(lawsonHelper.fillNull(lawsonHelper.getDisplayName(abContact)).replaceAll(regEx, ""), 80)).toUpperCase()  //LEGAL_NAME
      lawsonContactDTO.ACTIVITY = ''//ACTIVITY
      lawsonContactDTO.ACCT_CATEGORY = '' //ACCT_CATEGORY
      lawsonContactDTO.DISCOUNT_CODE = '' //DISCOUNT_CODE
      lawsonContactDTO.PRIME_RATE = 0 //PRIME_RATE
      lawsonContactDTO.INVOICE_GROUP = ''//INVOICE_GROUP
      lawsonContactDTO.ERS_CAPABLE = '' //ERS_CAPABLE
      lawsonContactDTO.INVC_REF_TYPE = '' //INVC_REF_TYPE
      lawsonContactDTO.EDI_AUTO_REL = ''//EDI_AUTO_REL
      lawsonContactDTO.AUTH_CODE = '' //AUTH_CODE
      lawsonContactDTO.CHARGE_CODE = ''//CHARGE_CODE
      lawsonContactDTO.PMT_CAT_CODE = lawsonHelper.fillNull(isBCContact ? paymentCatCode_BC : paymentCatCode_CC) //PMT_CAT_CODE
      lawsonContactDTO.NORM_EXP_CODE = '' //NORM_EXP_CODE
      lawsonContactDTO.PMT_FORM = '' //PMT_FORM
      lawsonContactDTO.SWIFT_ID = '' //SWIFT_ID
      lawsonContactDTO.PROC_GRP = ''//PROC_GRP
      lawsonContactDTO.MATCH_TABLE = '' //MATCH_TABLE
      lawsonContactDTO.HANDLING_CODE = '' //HANDLING_CODE
      lawsonContactDTO.DISC_CALC_DATE = '' //DISC_CALC_DATE
      lawsonContactDTO.ENCLOSURE = '' //ENCLOSURE
      lawsonContactDTO.CREATE_POD_FL = ''//CREATE_POD_FL
      lawsonContactDTO.REQ_MATCH_REF = '' //REQ_MATCH_REF
      lawsonContactDTO.POOL_OPTION = 0 //POOL_OPTION
      lawsonContactDTO.HOLD_INSP_FLAG = '' //HOLD_INSP_FLAG
      lawsonContactDTO.WRITE_OFF_AMT = 0 //WRITE_OFF_AMT
      lawsonContactDTO.VEN_CLAIM_TYPE = '' //VEN_CLAIM_TYPE
      lawsonContactDTO.CLAIM_HOLD_CD = '' //CLAIM_HOLD_CD
      lawsonContactDTO.CB_HOLD_CODE = '' //CB_HOLD_CODE
      lawsonContactDTO.CB_MINIMUM_AMT = 0  //CB_MINIMUM_AMT
      lawsonContactDTO.REPLACE_GOODS = '' //REPLACE_GOODS
      lawsonContactDTO.SHIP_OR_HOLD = ''//SHIP_OR_HOLD
      lawsonContactDTO.ERS_HANDLING = '' //ERS_HANDLING
      lawsonContactDTO.E_MAIL_ADDRESS = '' //E_MAIL_ADDRESS
      lawsonContactDTO.URL_ADDR = '' //URL_ADDR
      lawsonContactDTO.VEND_ACCT = '' //VEND_ACCT
      lawsonContactDTO.LANGUAGE_CODE = '' //LANGUAGE_CODE
      lawsonContactDTO.TAX_USAGE_CD = '' //TAX_USAGE_CD
      lawsonContactDTO.VAT_REG_CTRY = '' //VAT_REG_CTRY
      lawsonContactDTO.VAT_REG_NBR = '' //VAT_REG_NBR
      lawsonContactDTO.VALIDATE_PO = '' //VALIDATE_PO
      lawsonContactDTO.REQUIRE_PO = '' //REQUIRE_PO
      lawsonContactDTO.POV_BUYER_CODE = '' //POV_BUYER_CODE
      lawsonContactDTO.POV_OPEN_PO_LM = 0 //POV_OPEN_PO_LM
      lawsonContactDTO.POV_OPEN_PO_AM = 0 //POV_OPEN_PO_AM
      lawsonContactDTO.POV_LEADTIME = 0 //POV_LEADTIME
      lawsonContactDTO.POV_EDI_NUMBER = '' //POV_EDI_NUMBER
      lawsonContactDTO.POV_FAX_PREFIX = '' //POV_FAX_PREFIX
      lawsonContactDTO.POV_FAX_EXT = '' //POV_FAX_EXT
      lawsonContactDTO.POV_FAX_NUMBER = '' //POV_FAX_NUMBER
      lawsonContactDTO.POV_MAX_ORD_AM = 0 //POV_MAX_ORD_AM
      lawsonContactDTO.POV_FREIGHT_TR = ''//POV_FREIGHT_TR
      lawsonContactDTO.POV_SHIP_VIA = '' //POV_SHIP_VIA
      lawsonContactDTO.POV_FOB_CODE = '' //POV_FOB_CODE
      lawsonContactDTO.POV_NORM_DEL = ''//POV_NORM_DEL
      lawsonContactDTO.POV_OSHIP_TPCT = 0 //POV_OSHIP_TPCT
      lawsonContactDTO.POV_USHIP_TPCT = 0 //POV_USHIP_TPCT
      lawsonContactDTO.POV_HIN_NBR = ''//POV_HIN_NBR
      lawsonContactDTO.POV_ZOHSHIP_FL = '' //POV_ZOHSHIP_FL
      lawsonContactDTO.POV_ZUHSHIP_FL = '' //POV_ZUHSHIP_FL
      lawsonContactDTO.POV_CAN_ALL_BO = '' //POV_CAN_ALL_BO
      lawsonContactDTO.POV_FOB_CODE = '' //POV_RMA_REQ
      lawsonContactDTO.POV_RMA_REQ = ''
      lawsonContactDTO.POV_RMA_DOC_RQ = '' //POV_RMA_DOC_RQ
      lawsonContactDTO.POV_OV_SHP_VIA = '' //POV_OV_SHP_VIA
      lawsonContactDTO.POV_REQ_LOCLVL = '' //POV_REQ_LOCLVL
      lawsonContactDTO.POV_REQ_CONLVL = '' //POV_REQ_CONLVL
      lawsonContactDTO.POV_ISSUE_MTHD = '' //POV_ISSUE_MTHD
      lawsonContactDTO.POV_REV_ISS_MT = '' //POV_REV_ISS_MT
      lawsonContactDTO.POV_REV_EDI_NB = '' //POV_REV_EDI_NB
      lawsonContactDTO.POV_REV_EDI_TR = '' //POV_REV_EDI_TR
      lawsonContactDTO.POV_REV_INCL = '' //POV_REV_INCL
      lawsonContactDTO.POV_VEN_CONT = '' //POV_VEN_CONT
      lawsonContactDTO.POV_FOB_CODE = '' //POV_PHONE_PREF
      lawsonContactDTO.POV_PHONE_PREF = '' //POV_PHONE_NUM
      lawsonContactDTO.POV_PHONE_NUM = ''
      lawsonContactDTO.POV_PHONE_EXT = '' //POV_PHONE_EXT
      lawsonContactDTO.POV_VEND_ACCT = ''//POV_VEND_ACCT
      lawsonContactDTO.POV_PO_CODE = '' //POV_PO_CODE
      lawsonContactDTO.POV_UPD_PO_CST = 0 //POV_UPD_PO_CST
      lawsonContactDTO.POV_USR_FLD_01 = '' //POV_USR_FLD_01
      lawsonContactDTO.POV_USR_FLD_02 = '' //POV_USR_FLD_02
      lawsonContactDTO.POV_USR_FLD_03 = '' //POV_USR_FLD_03
      lawsonContactDTO.POV_USR_FLD_04 = '' //POV_USR_FLD_04
      lawsonContactDTO.POV_USR_FLD_05 = '' //POV_USR_FLD_05
      lawsonContactDTO.MBL_INT_PREFIX = '' //MBL_INT_PREFIX
      lawsonContactDTO.MOBILE_NUM = '' //MOBILE_NUM
      lawsonContactDTO.MOBILE_EXT = '' //MOBILE_EXT
      lawsonContactDTO.PAY_IMM_FLAG = '' //PAY_IMM_FLAG
      lawsonContactDTO.DIVERSE_CODE = '' //DIVERSE_CODE
      lawsonContactDTO.INTM_BANK_ENT = '' //INTM_BANK_ENT
      lawsonContactDTO.INTM_BANK_ACCT = '' //INTM_BANK_ACCT
      lawsonContactDTO.INTM_BANK_IDNT = '' //INTM_BANK_IDNT
      lawsonContactDTO.INTM_BANK_CURR = '' //INTM_BANK_CURR
      lawsonContactDTO.INTM_PRENOT = '' //INTM_PRENOT
      lawsonContactDTO.INTM_SWIFT_ID = '' //INTM_SWIFT_ID
      lawsonContactDTO.INTM_PMT_CAT = '' //INTM_PMT_CAT
      lawsonContactDTO.INTM_NORM_EXP = '' //INTM_NORM_EXP
      lawsonContactDTO.INTM_PMT_FORM = ''//INTM_PMT_FORM
      lawsonContactDTO.INTM_CHRG_CD = ''  //INTM_CHRG_CD
      lawsonContactDTO.INTM_RIBKEY = 0 //INTM_RIBKEY
      lawsonContactDTO.INTM_CRS_IDENT = '' //INTM_CRS_IDENT
      lawsonContactDTO.FLOAT_DAYS = 0 //FLOAT_DAYS
      lawsonContactDTO.VN_BLD_SOC_REF = '' //VN_BLD_SOC_REF
      lawsonContactDTO.VN_BLD_ACCT_NM = '' //VN_BLD_ACCT_NM
      lawsonContactDTO.IN_BLD_SOC_REF = '' //IN_BLD_SOC_REF
      lawsonContactDTO.IN_BLD_ACCT_NM = ''//IN_BLD_ACCT_NM
      lawsonContactDTO.MTCH_PREPAY_FL = '' //MTCH_PREPAY_FL
      lawsonContactDTO.MTCH_PREPAY_MT = 0 //MTCH_PREPAY_MT
      lawsonContactDTO.BUILD_SOC_REF = '' //BUILD_SOC_REF
      lawsonContactDTO.BUILD_ACCT_NM = '' //BUILD_ACCT_NM
      lawsonContactDTO.MIN_ORD_WEIGHT = 0 //MIN_ORD_WEIGHT
      lawsonContactDTO.UNLOADING_PORT = ''  //UNLOADING_PORT
      lawsonContactDTO.LOADING_PORT = ''  //LOADING_PORT
      lawsonContactDTO.WORKFLOW_GROUP = ''  //WORKFLOW_GROUP
      lawsonContactDTO.RULE_GROUP = '' //RULE_GROUP
      lawsonContactDTO.CROSS_IDENT = ''  //CROSS_IDENT
      lawsonContactDTO.GIRO_NUMBER = '' //GIRO_NUMBER
      lawsonContactDTO.CR_CARD_NUMBER = ''  //CR_CARD_NUMBER
      lawsonContactDTO.BANK_INSTRUCT1 = ''  //BANK_INSTRUCT1
      lawsonContactDTO.BANK_INSTRUCT2 = '' //BANK_INSTRUCT2
      lawsonContactDTO.BANK_INSTRUCT3 = '' //BANK_INSTRUCT3
      lawsonContactDTO.BANK_INSTRUCT4 = ''  //BANK_INSTRUCT4
      lawsonContactDTO.LOC_ID_PRT = 0 //LOC_ID_PRT
      lawsonContactDTO.LOC_ID_PRT = 0 //PUNCHOUT_CHG_F
      lawsonContactDTO.EMAIL_ADDRESS = '' //EMAIL_ADDRESS
      lawsonContactDTO.REORDER_SUN_FL = 0 //REORDER_SUN_FL
      lawsonContactDTO.REORDER_MON_FL = 0 //REORDER_MON_FL
      lawsonContactDTO.REORDER_TUE_FL = 0 //REORDER_TUE_FL
      lawsonContactDTO.REORDER_WED_FL = 0 //REORDER_WED_FL
      lawsonContactDTO.REORDER_THU_FL = 0 //REORDER_THU_FL
      lawsonContactDTO.REORDER_FRI_FL = 0 //REORDER_FRI_FL
      lawsonContactDTO.REORDER_SAT_FL = 0 //REORDER_SAT_FL
      lawsonContactDTO.DIVERSE_CODE5 = '' //DIVERSE_CODE5
      lawsonContactDTO.P_CARD_FLAG = 0 //P_CARD_FLAG
      lawsonContactDTO.PCARD_NBR = '' //PCARD_NBR
      lawsonContactDTO.VAL_DIV_DATE = '' //VAL_DIV_DATE
      lawsonContactDTO.POV_GTIN_USED = 0 //POV_GTIN_USED
      lawsonContactDTO.GTIN_ITEM_SYNC = 0 //GTIN_ITEM_SYNC
      lawsonContactDTO.AP_GLN_NBR = ''//AP_GLN_NBR
      lawsonContactDTO.PO_GLN_NBR = '' //PO_GLN_NBR
      lawsonContactDTO.SEC_WTH_EXEMPT = '' //SEC_WTH_EXEMPT
      lawsonContactDTO.SEC_WTH_CODE1 = '' //SEC_WTH_CODE1
      lawsonContactDTO.SEC_WTH_CODE2 = '' //SEC_WTH_CODE2
      lawsonContactDTO.SEC_WTH_CODE3 = '' //SEC_WTH_CODE3
      lawsonContactDTO.VALID_CERT_DT = '' //VALID_CERT_DT
      lawsonContactDTO.FOR_ECON_CODE = '' //FOR_ECON_CODE
      lawsonContactDTO.CARRIER_FLAG = '' //CARRIER_FLAG
      lawsonContactDTO.BANK_CODE = '' //BANK_CODE
      lawsonContactDTO.BANK_ID = '' //BANK_ID
      lawsonContactDTO.ASSIGNMENT_NBR = '' //ASSIGNMENT_NBR
      lawsonContactDTO.DEBITING_SIGN = '' //DEBITING_SIGN
      lawsonContactDTO.INTM_BANK_CODE = '' //INTM_BANK_CODE
      lawsonContactDTO.INTM_BANK_ID = '' //INTM_BANK_ID
      lawsonContactDTO.INTM_ASSIGN_NO = '' //INTM_ASSIGN_NO
      lawsonContactDTO.INTM_DEBITING = '' //INTM_DEBITING
      lawsonContactDTO.CHK_DIG_TYPE = '' //CHK_DIG_TYPE
      lawsonContactDTO.SEPA_FLAG = ''//SEPA_FLAG
      lawsonContactDTO.INTM_SEPA_FLAG = '' //INTM_SEPA_FLAG
    })
    _logger.debug("End of 'getNewlyAddedContacts()' method of TDIC_LawsonAPI")
    return listOfContNeedAddrID
  }

  @Throws(SOAPException, "If communication errors occur")
  @Returns("List of Contacts with updated Locations")
  function getAddressChngedContacts() : List<TDIC_ContactsWithAddrsChangeDTO> {
    _logger.debug("Started Executing 'getAddressChngedContacts' method of TDIC_LawsonAPI")
    // return all the contacts with indicator 'NeedVendorID' set to 'true'
    var needAddressChng = Query.make(entity.ABContact).compare(ABContact#NotifyAddressChange, Equals, true).select().toList()
    var needAddressChngList = new ArrayList<ABContact>(){}
    var threholdCount = ScriptParameters.getParameterValue("LawsonRequestThreshold") as int
    if(needAddressChng.Count >  threholdCount){
      for (elemIndex in 0..|threholdCount) {
        needAddressChngList.add(needAddressChng[elemIndex])
      }
      needAddressChng = needAddressChngList
    }
    var lawsonHelper = new TDIC_LawsonHelper()
    var listOfContsWithUpdtLoc = new ArrayList<TDIC_ContactsWithAddrsChangeDTO>()
    needAddressChng.each(\abContact -> {
      var formattedLines = lawsonHelper.formatLines(_truncLength, 4, {abContact.PrimaryAddress.AddressLine1, abContact.PrimaryAddress.AddressLine2, abContact.PrimaryAddress.AddressLine3})
      var lawsonLocationDTO = new TDIC_ContactsWithAddrsChangeDTO()
      listOfContsWithUpdtLoc.add(lawsonLocationDTO)
      var isBCContact = abContact.TagTypes.length == 1 && abContact.TagTypes.first() == ContactTagType.TC_CLIENT
      lawsonLocationDTO.VENDOR_GROUP = lawsonHelper.fillNull(vendorGroup) //VENDOR_GROUP
      var vendor_vname = ""
      if(abContact typeis ABCompany){
        vendor_vname = (lawsonHelper.limitLength(lawsonHelper.fillNull(abContact.Name).replaceAll("[^A-Z a-z 0-9]", ""), 30)).toUpperCase()
      } else {
        var person = abContact as ABPerson
        vendor_vname = "${lawsonHelper.fillNull(person.FirstName)} ${lawsonHelper.fillNull(person.MiddleName == null ? "" : person.MiddleName.charAt(0) as String)} ${lawsonHelper.fillNull(person.LastName)} ${lawsonHelper.fillNull(person.Suffix as String)} ${lawsonHelper.fillNull(person.Credential_TDIC as String)}"
        vendor_vname = (lawsonHelper.limitLength(vendor_vname.replaceAll("[^A-Z a-z 0-9]", ""), 30)).toUpperCase()
      }
      lawsonLocationDTO.VENDOR_VNAME = vendor_vname
      lawsonLocationDTO.TAX_ID = lawsonHelper.fillNull(abContact.TaxID) //TAX_ID
      lawsonLocationDTO.LEGAL_NAME = (lawsonHelper.limitLength(lawsonHelper.fillNull(lawsonHelper.getDisplayName(abContact)).replaceAll(regEx, ""), 80)).toUpperCase()  //getDisplayName(aContact))
      lawsonLocationDTO.VENDOR =  (abContact.VendorNumber.HasContent) ? String.format(vendorNumFormat, {abContact.VendorNumber}) : VENDOR_NUMBER_NOT_AVAILABLE + abContact.PublicID
      lawsonLocationDTO.OLD_VENDOR = (abContact.VendorNumber.HasContent) ? String.format(vendorNumFormat, {abContact.VendorNumber}) : VENDOR_NUMBER_NOT_AVAILABLE + abContact.PublicID
      lawsonLocationDTO.LOCATION_CODE = lawsonHelper.fillNull(isBCContact ? locationCode_BC : locationCode_CC) //INTM_BANK_ID
      lawsonLocationDTO.EFFECTIVE_DATE = (new SimpleDateFormat(dateFormat)).format(Calendar.getInstance().getTimeInMillis())
      lawsonLocationDTO.ADDR1 = lawsonHelper.fillNull(formattedLines[0])
      lawsonLocationDTO.ADDR2 = lawsonHelper.fillNull(formattedLines[1])
      lawsonLocationDTO.ADDR3 = lawsonHelper.fillNull(formattedLines[2])
      lawsonLocationDTO.ADDR4 = lawsonHelper.fillNull(formattedLines[3])
      lawsonLocationDTO.CITY_ADDR5 = lawsonHelper.fillNull(abContact.PrimaryAddress.City)
      lawsonLocationDTO.STATE_PROV = lawsonHelper.fillNull(abContact.PrimaryAddress.State.Code)
      lawsonLocationDTO.POSTAL_CODE = lawsonHelper.fillNull(abContact.PrimaryAddress.PostalCode)
      //GINTEG-1176 - Comment out county and country
      lawsonLocationDTO.COUNTY = '' //lawsonHelper.fillNull(abContact.PrimaryAddress.County)
      lawsonLocationDTO.COUNTRY_CODE = '' //lawsonHelper.fillNull(abContact.PrimaryAddress.Country.DisplayName)
      lawsonLocationDTO.REGION = ''
      lawsonLocationDTO.RESP_CODE = ''
      lawsonLocationDTO.CONTACT_LVL = ''
    })
    _logger.debug("End of 'getAddressChngedContacts' method of TDIC_LawsonAPI")
    return listOfContsWithUpdtLoc
  }

  @Throws(SOAPException, "If communication errors occur")
  @Throws(IllegalArgumentException, "If an illegal field value is supplied.")
  @Param("updateContactWithNewVendorDTO", "The PublicID of Contact to be updated")
  @Returns("List of Old Vendor Ids Which are not found")
  function updateVendorIDofContacts(updateContactWithNewVendorDTO : List<TDIC_UpdateContactWithNewVendorDTO>) : List<TDIC_OldVendorNotFoundDTO> {
    _logger.debug("Start of 'updateVendorIDofContacts' method of TDIC_LawsonAPI")
    var contactsNeedsVendorUpdate = Query.make(entity.ABContact).
        compareIn(ABContact#PublicID, updateContactWithNewVendorDTO*.OLD_VENDOR).select()
    var vendorIDNotFound = new ArrayList<TDIC_UpdateContactWithNewVendorDTO>()
    vendorIDNotFound.addAll(updateContactWithNewVendorDTO)
    updateContactWithNewVendorDTO.each(\elt -> {
      var abContact = contactsNeedsVendorUpdate.firstWhere(\elt1 -> elt1.PublicID == elt.OLD_VENDOR)
      if (abContact != null) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          bundle.add(abContact)
          abContact.VendorNumber = elt.NEW_VENDOR
          abContact.NeedVendorID = false
          bundle.commit()
        })
        vendorIDNotFound.remove(elt)
      }
    })
    if (vendorIDNotFound.Count > 0) {
      var listOfVendorIdsNotFound = new ArrayList<TDIC_OldVendorNotFoundDTO>()
      vendorIDNotFound.each(\elt -> {
        var notFound = new TDIC_OldVendorNotFoundDTO()
        notFound.OLD_VENDOR = elt.OLD_VENDOR
        listOfVendorIdsNotFound.add(notFound)
      })
      _logger.debug("End of 'updateVendorIDofContacts' method of TDIC_LawsonAPI")
      return listOfVendorIdsNotFound
    } else {
      _logger.debug("End of 'updateVendorIDofContacts' method of TDIC_LawsonAPI")
      return new TDIC_OldVendorNotFoundDTO[0].toList()
    }
  }

  @Throws(SOAPException, "If communication errors occur")
  @Throws(IllegalArgumentException, "If an illegal field value is supplied.")
  @Param("updateContactWithNewVendorDTO", "The PublicID of Contact to be updated")
  @Returns("boolean indicator to indicate whether update is done succesfully or not")
    // Search with Vendoe Number and update address change notif flag
  function acknowledgeAddressChange(ackAddressChangeDTO : List<TDIC_AckAddressChangeDTO>) : boolean {
    _logger.debug("Start of 'acknowledgeAddressChange' method of TDIC_LawsonAPI")
    var contactsNeedLocUpdAckn = Query.make(entity.ABContact).
        compareIn(ABContact#VendorNumber, ackAddressChangeDTO*.VENDOR).select()
    contactsNeedLocUpdAckn.each(\abContact -> {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        bundle.add(abContact)
        abContact.NotifyAddressChange = false
        //GINTEG-820 : ContactUpdate - NotifyAddressChange flag does not reset on acknowledgement for Address Change
        bundle.commit()
        abContact.Bundle.commit()
      })
    })
    _logger.debug("End of 'acknowledgeAddressChange' method of TDIC_LawsonAPI")
    //GINTEG-819 : If we are not finding records for all the VENDOR Id's received then return 'false'
    return contactsNeedLocUpdAckn.Count == ackAddressChangeDTO*.VENDOR.Count
  }
}