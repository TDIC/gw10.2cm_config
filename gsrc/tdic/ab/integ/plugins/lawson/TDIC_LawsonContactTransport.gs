package tdic.ab.integ.plugins.lawson

uses tdic.ab.integ.plugins.lawson.helper.TDIC_LawsonHelper
uses gw.api.system.PLLoggerCategory
uses tdic.ab.integ.plugins.message.TDIC_MessagePlugin

class TDIC_LawsonContactTransport extends TDIC_MessagePlugin {
// Sudheendra : Logger for Lawson Contact Update
  private static var _logger = PLLoggerCategory.TDIC_LAWSONCONTACTUPD
  private static final var MSG_VENDOR_NUMBER_NOT_FOUND = "Contact doesn't have Vendor Number."
  override function send(aMessage : entity.Message, aPayload : String) {
    _logger.debug("TDIC_LawsonContactTransport#send() - Entering.")
    _logger.debug("TDIC_LawsonContactTransport#send() - Executing SQL: " + aPayload)
    try {
      if(aPayload.contains(TDIC_LawsonHelper.VENDOR_NUMBER_NOT_AVAILABLE)){
        reportError(aMessage, MSG_VENDOR_NUMBER_NOT_FOUND)
        return
      }
      var helper = new TDIC_LawsonHelper()
      var _ret = helper.execute(aPayload)
      if (_ret > 0) {
        _logger.debug("TDIC_LawsonContactTransport#send() - SQL execution successful")
        aMessage.reportAck()
      } else {
        reportError(aMessage, "Lawson returned code: ${_ret}")
      }
    } catch (e) {
      reportError(aMessage, e.Message)
      _logger.error("TDIC_LawsonContactTransport#send() - Exception: " + e.Message)
    }
  }

  override function shutdown() {
    _logger.debug("TDIC_LawsonContactTransport#shutdown() - Entering.")
    // Do Nothing
    _logger.debug("TDIC_LawsonContactTransport#shutdown() - Exiting.")
  }

  override function resume() {
    _logger.debug("TDIC_LawsonContactTransport#resume() - Entering.")
    // Do Nothing
    _logger.debug("TDIC_LawsonContactTransport#resume() - Exiting.")
  }

  /**
   * US556
   * 03/27/2015 Shane Murphy
   *
   * This method changes the wait time while retry
   * For each retry time varies.
   */
  function reportError(aMessage : entity.Message, errorMessage : String) {
      aMessage.reportError()
      aMessage.ErrorDescription = errorMessage
  }
}