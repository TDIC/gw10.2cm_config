package tdic.ab.integ.plugins.lawson.helper

uses gw.api.util.DateUtil
uses gw.pl.logging.LoggerCategory
uses tdic.util.cache.CacheManager
uses com.tdic.util.database.DatabaseManager

uses java.io.IOException
uses java.lang.Exception
uses java.sql.Connection
uses java.sql.SQLException
uses java.util.ArrayList
uses java.lang.IllegalStateException
uses java.util.Calendar
uses java.text.SimpleDateFormat

/**
 * US556
 * 01/29/2015 Shane Murphy
 *
 * Helper Class for the ContactSync Lawson Integration
 */
class TDIC_LawsonHelper {
// Sudheendra : Logger for Lawson Contact Update
  static final var _logger = LoggerCategory.TDIC_LAWSONCONTACTUPD
  static final var vendorGroup = "VMST"
  static final var locationCode = 80
  static final var locType = "B"
  static final var venClass = "NR"
  static final var _truncLength = 30
  static final var _nullFill = ""
  public static final var VENDOR_NUMBER_NOT_AVAILABLE: String = "VendorNumber not available for Contact"
  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Returns a connection to Lawson
   */
  @Returns("Connection")
  public static function getConnection() : Connection {
    _logger.info("TDIC_LawsonHelper#getLawsonConnection() - Entering")
    var _lawsonDb = CacheManager.AppDatabaseCache.get("Lawson")
    if (_lawsonDb == null){
      throw new IllegalStateException("No Lawson App Database information found.")
    }
    try {
      _logger.info("TDIC_LawsonHelper#getLawsonConnection() - Exiting")
      return DatabaseManager.getConnection(_lawsonDb.Host,
          _lawsonDb.Instance,
          _lawsonDb.Port,
          _lawsonDb.DbName,
          _lawsonDb.UserName,
          _lawsonDb.Password)
    } catch (io : IOException) {
      _logger.error("TDIC_LawsonHelper#getLawsonConnection() - IOException - Cannot establish connection: " + io)
      throw(io)
    } catch (sql : SQLException) {
      _logger.error("TDIC_LawsonHelper#getLawsonConnection() - SQLException - Cannot establish connection: " + sql)
      throw(sql)
    } catch (e : Exception) {
      _logger.error("TDIC_LawsonHelper#getLawsonConnection() - Exception - Cannot establish connection: " + e)
      throw(e)
    }
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Formats a company's name and address for storage in lawson, returning
   * each address line as a value in a String[]
   */
  @Param("truncLength", "The max length of each array element")
  @Param("aContact", "The contact to format the address of")
  @Param("minNumLines", "The minimum number of entries in the array we need (To prevent NPE)")
  @Returns("Formatted Address?.asArrayOf(String)")
  function getFormattedComanyAddress(truncLength : int, minNumLines : int, aContact : ABCompany) : String[] {
    return formatLines(truncLength, minNumLines, {fillNull(aContact.Name).replaceAll("[^A-Z a-z 0-9]", ""), fillNull(aContact.PrimaryAddress?.AddressLine1), fillNull(aContact.PrimaryAddress?.AddressLine2), fillNull(aContact.PrimaryAddress?.AddressLine3)})
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Formats a persons's name and address for storage in lawson, returning
   * each address line as a value in a String[]
   */
  @Param("truncLength", "The max length of each array element")
  @Param("aContact", "The contact to format the address of")
  @Param("minNumLines", "The minimum number of entries in the array we need (To prevent NPE)")
  @Returns("Formatted Address?.asArrayOf(String)")
  function getFormattedPersonAddress(truncLength : int, minNumLines : int, aContact : ABPerson) : String[] {
    var middleInitial = aContact.MiddleName == null ? "" : aContact.MiddleName.charAt(0) as String
    var contactName = "${fillNull(aContact.FirstName)} ${fillNull(middleInitial)} ${fillNull(aContact.LastName)} ${fillNull(aContact.Suffix as String)} ${fillNull(aContact.Credential_TDIC as String)}"
    return formatLines(truncLength, minNumLines, {contactName.replaceAll("[^A-Z a-z 0-9]", ""), fillNull(aContact.PrimaryAddress?.AddressLine1), fillNull(aContact.PrimaryAddress?.AddressLine2), fillNull(aContact.PrimaryAddress?.AddressLine3)})
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Executes an update on both tables
   */
  @Param("aContact", "The Contact to update")
  @Returns(">1 if successful")
  function execute(sql : String) : int {
    _logger.info("TDIC_LawsonHelper#execute() - Entering")
    var conn = getConnection()
    var _ret : int
    try {
      _ret = DatabaseManager.executeStmt(conn, sql)
      _logger.info("TDIC_LawsonHelper#execute() - returned: " + _ret)
    } catch (sqlException : SQLException) {
      _logger.error("TDIC_LawsonHelper#getLawsonConnection() - SQLException executing statement: " + sqlException)
    } catch (exception : Exception) {
      _logger.error("TDIC_LawsonHelper#getLawsonConnection() - Exception executing statement: " + exception)
    } finally {
      DatabaseManager.closeConnection(conn)
    }
    _logger.info("TDIC_LawsonHelper#execute() - Exiting")
    return _ret
  }

  /**
   * US556
   * 03/26/2015 Shane Murphy
   *
   * Ensure we always insert an empty string instead of null
   */
  @Param("value", "Value to convert to empty string if nul")
  @Returns("Value passed in, or if null, an empty string")
  function fillNull(value : String) : String {
    return value == null ? _nullFill : value
  }

  /**
   *
   * Ensure string is of a maximum length...
   */
  function limitLength(value : String, lenLimit : int) : String {
    if( value == null ) {
      return _nullFill
    }
    if( value.length <= lenLimit ) {
      return value
    }
    return value.substring(0, lenLimit)
  }

  /**
   *
   * Escape before inserting into database.
   */
  function escapeForSql(value : String) : String {
    return value == null ? "" : value.replace("'", "''")
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Returns the update string for a contact on the APCVENADDR table
   */
  @Param("aContact", "Contact to retrieve update string for")
  @Returns("APCVENADDR Update String")
  function getVenAddUpdateString(aContact : ABContact) : String {
    _logger.info("TDIC_LawsonHelper#getVenAddUpdateString() - Entering")
    var formattedLines = formatLines(_truncLength, 4, {aContact.PrimaryAddress.AddressLine1, aContact.PrimaryAddress.AddressLine2, aContact.PrimaryAddress.AddressLine3})
    var vendorHelper = new TDIC_Vendor()
    // var vendorNumber = aContact.VendorNumber != null ? aContact.VendorNumber : vendorHelper.getVendorNumberFromLawson(aContact.TaxID)
    _logger.info("TDIC_LawsonHelper#getVenAddUpdateString() - Exiting")
    var insertString = "INSERT INTO APCVENADDR (\"VENDOR_GROUP\",\"VENDOR\",\"LOCATION_CODE\",\"EFFECTIVE_DATE\","
        +"\"ADDR1\",\"ADDR2\",\"ADDR3\",\"ADDR4\",\"CITY_ADDR5\",\"STATE_PROV\",\"POSTAL_CODE\","
        +"\"COUNTY\",\"COUNTRY\",\"COUNTRY_CODE\",\"REGION\",\"RESP_CODE\",\"CONTACT_LVL\")"
        +" VALUES ('${fillNull(vendorGroup)}','${formatVendorNumber(aContact.VendorNumber, aContact.PublicID)}','${fillNull(locationCode as String)}','"
        +(new SimpleDateFormat("yyyyMMdd")).format(Calendar.getInstance().getTimeInMillis())+"','${fillNull(formattedLines[0])}','${fillNull(formattedLines[1])}',"
        +"'${fillNull(formattedLines[2])}','${fillNull(formattedLines[3])}','${fillNull(aContact.PrimaryAddress.City)}',"
        +"'${fillNull(aContact.PrimaryAddress.State.Code)}','${fillNull(aContact.PrimaryAddress.PostalCode)}',"
        +"'${fillNull(aContact.PrimaryAddress.County)}','${fillNull(aContact.PrimaryAddress.Country.DisplayName)}',"
        +"'${fillNull(aContact.PrimaryAddress.Country.Code)}','','',0)"

    _logger.info("TDIC_LawsonHelper#getVenAddUpdateString() - Insert String: " + insertString)
    _logger.info("TDIC_LawsonHelper#getVenAddUpdateString() - Exiting")
    return insertString
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Returns the update string for a contact on the APCVENMAST table
   */
  @Param("aContact", "Contact to retrieve update string for")
  @Returns("APCVENADDR Update String")
  function getDefaultVenAddUpdateString(aContact : ABContact) : String {
    _logger.info("TDIC_LawsonHelper#getDefaultVenAddUpdateString() - Entering")
    var formattedLines = formatLines(_truncLength, 4, {aContact.PrimaryAddress.AddressLine1,
        aContact.PrimaryAddress.AddressLine2, aContact.PrimaryAddress.AddressLine3})

    _logger.info("TDIC_LawsonHelper#getDefaultVenAddUpdateString() - Exiting")
    var updateString = "UPDATE APCVENMAST SET ADDR1='" + fillNull(formattedLines[0]) + "',ADDR2='" + fillNull
        (formattedLines[1]) + "',ADDR3='" + fillNull(formattedLines[2]) +
        "',ADDR4='" + fillNull(formattedLines[3]) + "',CITY_ADDR5='" + fillNull(aContact.PrimaryAddress.City) +
        "',STATE_PROV='" + fillNull(aContact.PrimaryAddress.State as java.lang.String) +
        "',POSTAL_CODE='" + fillNull(aContact.PrimaryAddress.PostalCode) + "',COUNTY='" + fillNull
            (aContact.PrimaryAddress.County) + "',COUNTRY='" + fillNull(aContact.PrimaryAddress.Country.Code) +
        "' WHERE TAX_ID='" + fillNull(aContact.TaxID) + "'"

    _logger.info("TDIC_LawsonHelper#getDefaultVenAddUpdateString() - Update String: " + updateString)
    _logger.info("TDIC_LawsonHelper#getDefaultVenAddUpdateString() - Exiting")
    return updateString
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Returns the create string for a contact on the APCVENMAST table. All 204 fields on this table are nonnullable
   */
  @Param("aContact", "Contact to retrieve create string for")
  @Returns("APCVENMAST Update String")
  function getVenMastCreateStringLocationRecord(aContact : ABContact) : String {
    _logger.info("TDIC_LawsonHelper#getVenMastCreateStringLocationRecord() - Entering")
    var formattedLines = aContact typeis ABCompany ?
        getFormattedComanyAddress(_truncLength, 5, aContact) :
        getFormattedPersonAddress(_truncLength, 5, (aContact as ABPerson))
    var createString = "INSERT INTO APCVENMAST (" +
        "\"VENDOR_GROUP\"," + "\"VENDOR\"," + "\"OLD_VENDOR\"," + "\"LOCATION_CODE\"," + "\"LOC_TYPE\"," + "\"VEN_CLASS\"," + "\"VENDOR_VNAME\"," + "\"VENDOR_SNAME\"," + "\"VENDOR_CONTCT\"," + "\"ADDR1\"," +
        "\"ADDR2\"," + "\"ADDR3\"," + "\"ADDR4\"," + "\"CITY_ADDR5\"," + "\"STATE_PROV\"," + "\"POSTAL_CODE\"," + "\"COUNTY\"," + "\"COUNTRY\"," + "\"REGION\"," + "\"COUNTRY_CODE\"," +
        "\"NORM_REMIT\"," + "\"NORM_PURCH\"," + "\"REMIT_TO_CODE\"," + "\"PURCH_FR_LOC\"," + "\"PAY_VENDOR\"," + "\"VENDOR_STATUS\"," + "\"VEN_PRIORITY\"," + "\"PHONE_PREFIX\"," + "\"PHONE_NUM\"," + "\"PHONE_EXT\"," +
        "\"FAX_PREFIX\"," + "\"FAX_NUM\"," + "\"FAX_EXT\"," + "\"TELEX_NUM\"," + "\"TERM_CODE\"," + "\"INV_CURRENCY\"," + "\"BAL_CURRENCY\"," + "\"BANK_CURRENCY\"," + "\"CURR_RECALC\"," + "\"SEP_CHK_FLAG\"," +
        "\"TAX_ID\"," + "\"TAX_CODE\"," + "\"HLD_CODE\"," + "\"DIST_CODE\"," + "\"ACCR_CODE\"," + "\"BANK_INST_CODE\"," + "\"CASH_CODE\"," + "\"BANK_ENTITY\"," + "\"VBANK_ACCT_NO\"," + "\"VBANK_IDENT\"," +
        "\"RIBKEY\"," + "\"VBANK_ACCT_TP\"," + "\"INCOME_CODE\"," + "\"INCOME_WH_FLG\"," + "\"EDI_NBR\"," + "\"ACH_PRENOT\"," + "\"MAX_INV_AMT\"," + "\"ORIGIN_DATE\"," + "\"USER_NAME_01\"," + "\"USER_NAME_02\"," +
        "\"USER_NAME_03\"," + "\"USER_NAME_04\"," + "\"USER_NAME_05\"," + "\"USER_NAME_06\"," + "\"CUST_GROUP\"," + "\"CUSTOMER\"," + "\"LEGAL_NAME\"," + "\"ACTIVITY\"," + "\"ACCT_CATEGORY\"," + "\"DISCOUNT_CODE\"," +
        "\"PRIME_RATE\"," + "\"INVOICE_GROUP\"," + "\"ERS_CAPABLE\"," + "\"INVC_REF_TYPE\"," + "\"EDI_AUTO_REL\"," + "\"AUTH_CODE\"," + "\"CHARGE_CODE\"," + "\"PMT_CAT_CODE\"," + "\"NORM_EXP_CODE\"," + "\"PMT_FORM\"," +
        "\"SWIFT_ID\"," + "\"PROC_GRP\"," + "\"MATCH_TABLE\"," + "\"HANDLING_CODE\"," + "\"DISC_CALC_DATE\"," + "\"ENCLOSURE\"," + "\"CREATE_POD_FL\"," + "\"REQ_MATCH_REF\"," + "\"POOL_OPTION\"," + "\"HOLD_INSP_FLAG\"," +
        "\"WRITE_OFF_AMT\"," + "\"VEN_CLAIM_TYPE\"," + "\"CLAIM_HOLD_CD\"," + "\"CB_HOLD_CODE\"," + "\"CB_MINIMUM_AMT\"," + "\"REPLACE_GOODS\"," + "\"SHIP_OR_HOLD\"," + "\"ERS_HANDLING\"," + "\"E_MAIL_ADDRESS\"," + "\"URL_ADDR\"," +
        "\"VEND_ACCT\"," + "\"LANGUAGE_CODE\"," + "\"TAX_USAGE_CD\"," + "\"VAT_REG_CTRY\"," + "\"VAT_REG_NBR\"," + "\"VALIDATE_PO\"," + "\"REQUIRE_PO\"," + "\"POV_BUYER_CODE\"," + "\"POV_OPEN_PO_LM\"," + "\"POV_OPEN_PO_AM\"," +
        "\"POV_LEADTIME\"," + "\"POV_EDI_NUMBER\"," + "\"POV_FAX_PREFIX\"," + "\"POV_FAX_EXT\"," + "\"POV_FAX_NUMBER\"," + "\"POV_MAX_ORD_AM\"," + "\"POV_FREIGHT_TR\"," + "\"POV_SHIP_VIA\"," + "\"POV_FOB_CODE\"," + "\"POV_NORM_DEL\"," +
        "\"POV_OSHIP_TPCT\"," + "\"POV_USHIP_TPCT\"," + "\"POV_HIN_NBR\"," + "\"POV_ZOHSHIP_FL\"," + "\"POV_ZUHSHIP_FL\"," + "\"POV_CAN_ALL_BO\"," + "\"POV_RMA_REQ\"," + "\"POV_RMA_DOC_RQ\"," + "\"POV_OV_SHP_VIA\"," + "\"POV_REQ_LOCLVL\"," +
        "\"POV_REQ_CONLVL\"," + "\"POV_ISSUE_MTHD\"," + "\"POV_REV_ISS_MT\"," + "\"POV_REV_EDI_NB\"," + "\"POV_REV_EDI_TR\"," + "\"POV_REV_INCL\"," + "\"POV_VEN_CONT\"," + "\"POV_PHONE_PREF\"," + "\"POV_PHONE_NUM\"," + "\"POV_PHONE_EXT\"," +
        "\"POV_VEND_ACCT\"," + "\"POV_PO_CODE\"," + "\"POV_UPD_PO_CST\"," + "\"POV_USR_FLD_01\"," + "\"POV_USR_FLD_02\"," + "\"POV_USR_FLD_03\"," + "\"POV_USR_FLD_04\"," + "\"POV_USR_FLD_05\"," + "\"MBL_INT_PREFIX\"," + "\"MOBILE_NUM\"," +
        "\"MOBILE_EXT\"," + "\"PAY_IMM_FLAG\"," + "\"DIVERSE_CODE\"," + "\"INTM_BANK_ENT\"," + "\"INTM_BANK_ACCT\"," + "\"INTM_BANK_IDNT\"," + "\"INTM_BANK_CURR\"," + "\"INTM_PRENOT\"," + "\"INTM_SWIFT_ID\"," + "\"INTM_PMT_CAT\"," +
        "\"INTM_NORM_EXP\"," + "\"INTM_PMT_FORM\"," + "\"INTM_CHRG_CD\"," + "\"INTM_RIBKEY\"," + "\"INTM_CRS_IDENT\"," + "\"FLOAT_DAYS\"," + "\"VN_BLD_SOC_REF\"," + "\"VN_BLD_ACCT_NM\"," + "\"IN_BLD_SOC_REF\"," + "\"IN_BLD_ACCT_NM\"," +
        "\"MTCH_PREPAY_FL\"," + "\"MTCH_PREPAY_MT\"," + "\"BUILD_SOC_REF\"," + "\"BUILD_ACCT_NM\"," + "\"MIN_ORD_WEIGHT\"," + "\"UNLOADING_PORT\"," + "\"LOADING_PORT\"," + "\"WORKFLOW_GROUP\"," + "\"RULE_GROUP\"," + "\"CROSS_IDENT\"," +
        "\"GIRO_NUMBER\"," + "\"CR_CARD_NUMBER\"," + "\"BANK_INSTRUCT1\"," + "\"BANK_INSTRUCT2\"," + "\"BANK_INSTRUCT3\"," + "\"BANK_INSTRUCT4\"," + "\"LOC_ID_PRT\"," + "\"PUNCHOUT_CHG_F\"," + "\"EMAIL_ADDRESS\"," + "\"REORDER_SUN_FL\"," +
        "\"REORDER_MON_FL\"," + "\"REORDER_TUE_FL\"," + "\"REORDER_WED_FL\"," + "\"REORDER_THU_FL\"," + "\"REORDER_FRI_FL\"," + "\"REORDER_SAT_FL\"," + "\"DIVERSE_CODE5\"," + "\"P_CARD_FLAG\"," + "\"PCARD_NBR\"," + "\"VAL_DIV_DATE\"," +
        "\"POV_GTIN_USED\"," + "\"GTIN_ITEM_SYNC\"," + "\"AP_GLN_NBR\"," + "\"PO_GLN_NBR\"," + "\"SEC_WTH_EXEMPT\"," + "\"SEC_WTH_CODE1\"," + "\"SEC_WTH_CODE2\"," + "\"SEC_WTH_CODE3\"," + "\"VALID_CERT_DT\"," + "\"FOR_ECON_CODE\"," +
        "\"CARRIER_FLAG\"," + "\"BANK_CODE\"," + "\"BANK_ID\"," + "\"ASSIGNMENT_NBR\"," + "\"DEBITING_SIGN\"," + "\"INTM_BANK_CODE\"," + "\"INTM_BANK_ID\"," + "\"INTM_ASSIGN_NO\"," + "\"INTM_DEBITING\"," + "\"CHK_DIG_TYPE\"," +
        "\"SEPA_FLAG\"," + "\"INTM_SEPA_FLAG\")" + " VALUES ('" +
        fillNull(vendorGroup) + "','" + //VENDOR_GROUP
        '' + "','" + //VENDOR
        aContact.PublicID + "','" + //OLD_VENDOR
        fillNull(locationCode as String) + "','" + //LOCATION_CODE
        fillNull(locType) + "','" + //LOC_TYPE
        fillNull(venClass) + "','" + //VEN_CLASS
        escapeForSql(limitLength(fillNull(formattedLines[0]), 30)).toUpperCase() + "','" + //VENDOR_VNAME
        '' + "','" + //VENDOR_SNAME
        '' + "','" + //VENDOR_CONTCT
        escapeForSql(limitLength(fillNull(formattedLines[1]), 30)).toUpperCase() + "','" + //ADDR1
        escapeForSql(limitLength(fillNull(formattedLines[2]), 30)).toUpperCase() + "','" + //ADDR2
        escapeForSql(limitLength(fillNull(formattedLines[3]), 30)).toUpperCase() + "','" + // ADDR3
        escapeForSql(limitLength(fillNull(formattedLines[4]), 30)).toUpperCase() + "','" + //ADDR4
        escapeForSql(limitLength(fillNull(aContact.PrimaryAddress.City), 18)).toUpperCase() + "','" + //CITY_ADDR5
        fillNull(aContact.PrimaryAddress.State as String).toUpperCase() + "','" + //STATE_PROV
        fillNull(aContact.PrimaryAddress.PostalCode).toUpperCase() + "','" + //POSTAL_CODE
        escapeForSql(limitLength(fillNull(aContact.PrimaryAddress.County), 25)).toUpperCase() + "','" + //COUNTY
        fillNull(aContact.PrimaryAddress.Country.Code).toUpperCase() + "','" + //COUNTRY
        '' + "','" + //REGION
        '' + "','" + //COUNTRY_CODE
        '' + "','" + //NORM_REMIT
        '' + "','" + //NORM_PURCH
        '' + "','" + //REMIT_TO_CODE
        '' + "','" + //PURCH_FR_LOC
        '' + "','" + //PAY_VENDOR
        '' + "','" + //VENDOR_STATUS
        0 + "','" + //VEN_PRIORITY
        '' + "','" + //PHONE_PREFIX
        '' + "','" + //PHONE_NUM
        '' + "','" + //PHONE_EXT
        '' + "','" + //FAX_PREFIX
        '' + "','" + //FAX_NUM
        '' + "','" + //FAX_EXT
        '' + "','" + //TELEX_NUM
        'NET0' + "','" + //TERM_CODE
        '' + "','" + //INV_CURRENCY
        '' + "','" + //BAL_CURRENCY
        '' + "','" + //BANK_CURRENCY
        '' + "','" + //CURR_RECALC
        '' + "','" + //SEP_CHK_FLAG
        fillNull(aContact.TaxID) + "','" + //TAX_ID
        '' + "','" + //TAX_CODE
        '' + "','" + //HLD_CODE
        '' + "','" + //DIST_CODE
        '' + "','" + //ACCR_CODE
        'RFW' + "','" + //BANK_INST_CODE
        '0510' + "','" + //CASH_CODE
        '' + "','" + //BANK_ENTITY
        '' + "','" + //VBANK_ACCT_NO
        '' + "','" + //VBANK_IDENT
        0 + "','" + //RIBKEY
        '' + "','" + //VBANK_ACCT_TP
        '' + "','" + //INCOME_CODE
        '' + "','" + //INCOME_WH_FLG
        '' + "','" + //EDI_NBR
        '' + "','" + //ACH_PRENOT
        0 + "','" + //MAX_INV_AMT
        fillNull(DateUtil.currentDate().toSQLDate() as String) + "','" + //ORIGIN_DATE
        fillNull(aContact.PublicID) + "','" + //USER_NAME_01 = PublicID
        '' + "','" + //USER_NAME_02 = TaxID (FEIN/SSN)
        '' + "','" + //fillNull(aContact.OfficialIDs.firstWhere(\ elt -> elt.OfficialIDType == OfficialIDType.TC_PERSONFEIN_TDIC).OfficialIDValue) + "','" + //USER_NAME_03 = PersonFEIN_TDIC
        '' + "','" + //fillNull(aContact.OfficialIDs.firstWhere(\ elt -> elt.OfficialIDType == OfficialIDType.TC_SSN).OfficialIDValue) + "','" + //USER_NAME_04 = SSN
        '' + "','" + //fillNull(aContact.OfficialIDs.firstWhere(\ elt -> elt.OfficialIDType == OfficialIDType.TC_STAX).OfficialIDValue) + "','" + //USER_NAME_05 = STax
        '' + "','" + //fillNull(aContact.OfficialIDs.firstWhere(\ elt -> elt.OfficialIDType == OfficialIDType.TC_BUREAUID).OfficialIDValue) + "','" + //USER_NAME_06 = BureauID
        '' + "','" + //CUST_GROUP
        '' + "','" + //CUSTOMER
        escapeForSql(limitLength(fillNull(getDisplayName(aContact)).replaceAll("[^0-9 A-Z a-z]", ""),80)).toUpperCase() + "','" + //LEGAL_NAME
        '' + "','" + //ACTIVITY
        '' + "','" + //ACCT_CATEGORY
        '' + "','" + //DISCOUNT_CODE
        0 + "','" + //PRIME_RATE
        '' + "','" + //INVOICE_GROUP
        '' + "','" + //ERS_CAPABLE
        '' + "','" + //INVC_REF_TYPE
        '' + "','" + //EDI_AUTO_REL
        '' + "','" + //AUTH_CODE
        '' + "','" + //CHARGE_CODE
        '' + "','" + //PMT_CAT_CODE
        '' + "','" + //NORM_EXP_CODE
        '' + "','" + //PMT_FORM
        '' + "','" + //SWIFT_ID
        '' + "','" + //PROC_GRP
        '' + "','" + //MATCH_TABLE
        '' + "','" + //HANDLING_CODE
        '' + "','" + //DISC_CALC_DATE
        '' + "','" + //ENCLOSURE
        '' + "','" + //CREATE_POD_FL
        '' + "','" + //REQ_MATCH_REF
        0 + "','" + //POOL_OPTION
        '' + "','" + //HOLD_INSP_FLAG
        0 + "','" + //WRITE_OFF_AMT
        '' + "','" + //VEN_CLAIM_TYPE
        '' + "','" + //CLAIM_HOLD_CD
        '' + "','" + //CB_HOLD_CODE
        0 + "','" + //CB_MINIMUM_AMT
        '' + "','" + //REPLACE_GOODS
        '' + "','" + //SHIP_OR_HOLD
        '' + "','" + //ERS_HANDLING
        '' + "','" + //E_MAIL_ADDRESS
        '' + "','" + //URL_ADDR
        '' + "','" + //VEND_ACCT
        '' + "','" + //LANGUAGE_CODE
        '' + "','" + //TAX_USAGE_CD
        '' + "','" + //VAT_REG_CTRY
        '' + "','" + //VAT_REG_NBR
        '' + "','" + //VALIDATE_PO
        '' + "','" + //REQUIRE_PO
        '' + "','" + //POV_BUYER_CODE
        0 + "','" + //POV_OPEN_PO_LM
        0 + "','" + //POV_OPEN_PO_AM
        0 + "','" + //POV_LEADTIME
        '' + "','" + //POV_EDI_NUMBER
        '' + "','" + //POV_FAX_PREFIX
        '' + "','" + //POV_FAX_EXT
        '' + "','" + //POV_FAX_NUMBER
        0 + "','" + //POV_MAX_ORD_AM
        '' + "','" + //POV_FREIGHT_TR
        '' + "','" + //POV_SHIP_VIA
        '' + "','" + //POV_FOB_CODE
        '' + "','" + //POV_NORM_DEL
        0 + "','" + //POV_OSHIP_TPCT
        0 + "','" + //POV_USHIP_TPCT
        '' + "','" + //POV_HIN_NBR
        '' + "','" + //POV_ZOHSHIP_FL
        '' + "','" + //POV_ZUHSHIP_FL
        '' + "','" + //POV_CAN_ALL_BO
        '' + "','" + //POV_RMA_REQ
        '' + "','" + //POV_RMA_DOC_RQ
        '' + "','" + //POV_OV_SHP_VIA
        '' + "','" + //POV_REQ_LOCLVL
        '' + "','" + //POV_REQ_CONLVL
        '' + "','" + //POV_ISSUE_MTHD
        '' + "','" + //POV_REV_ISS_MT
        '' + "','" + //POV_REV_EDI_NB
        '' + "','" + //POV_REV_EDI_TR
        '' + "','" + //POV_REV_INCL
        '' + "','" + //POV_VEN_CONT
        '' + "','" + //POV_PHONE_PREF
        '' + "','" + //POV_PHONE_NUM
        '' + "','" + //POV_PHONE_EXT
        '' + "','" + //POV_VEND_ACCT
        '' + "','" + //POV_PO_CODE
        0 + "','" + //POV_UPD_PO_CST
        '' + "','" + //POV_USR_FLD_01
        '' + "','" + //POV_USR_FLD_02
        '' + "','" + //POV_USR_FLD_03
        '' + "','" + //POV_USR_FLD_04
        '' + "','" + //POV_USR_FLD_05
        '' + "','" + //MBL_INT_PREFIX
        '' + "','" + //MOBILE_NUM
        '' + "','" + //MOBILE_EXT
        '' + "','" + //PAY_IMM_FLAG
        '' + "','" + //DIVERSE_CODE
        '' + "','" + //INTM_BANK_ENT
        '' + "','" + //INTM_BANK_ACCT
        '' + "','" + //INTM_BANK_IDNT
        '' + "','" + //INTM_BANK_CURR
        '' + "','" + //INTM_PRENOT
        '' + "','" + //INTM_SWIFT_ID
        '' + "','" + //INTM_PMT_CAT
        '' + "','" + //INTM_NORM_EXP
        '' + "','" + //INTM_PMT_FORM
        '' + "','" + //INTM_CHRG_CD
        0 + "','" + //INTM_RIBKEY
        '' + "','" + //INTM_CRS_IDENT
        0 + "','" + //FLOAT_DAYS
        '' + "','" + //VN_BLD_SOC_REF
        '' + "','" + //VN_BLD_ACCT_NM
        '' + "','" + //IN_BLD_SOC_REF
        '' + "','" + //IN_BLD_ACCT_NM
        '' + "','" + //MTCH_PREPAY_FL
        0 + "','" + //MTCH_PREPAY_MT
        '' + "','" + //BUILD_SOC_REF
        '' + "','" + //BUILD_ACCT_NM
        0 + "','" + //MIN_ORD_WEIGHT
        '' + "','" + //UNLOADING_PORT
        '' + "','" + //LOADING_PORT
        '' + "','" + //WORKFLOW_GROUP
        '' + "','" + //RULE_GROUP
        '' + "','" + //CROSS_IDENT
        '' + "','" + //GIRO_NUMBER
        '' + "','" + //CR_CARD_NUMBER
        '' + "','" + //BANK_INSTRUCT1
        '' + "','" + //BANK_INSTRUCT2
        '' + "','" + //BANK_INSTRUCT3
        '' + "','" + //BANK_INSTRUCT4
        0 + "','" + //LOC_ID_PRT
        0 + "','" + //PUNCHOUT_CHG_F
        '' + "','" + //EMAIL_ADDRESS
        0 + "','" + //REORDER_SUN_FL
        0 + "','" + //REORDER_MON_FL
        0 + "','" + //REORDER_TUE_FL
        0 + "','" + //REORDER_WED_FL
        0 + "','" + //REORDER_THU_FL
        0 + "','" + //REORDER_FRI_FL
        0 + "','" + //REORDER_SAT_FL
        '' + "','" + //DIVERSE_CODE5
        0 + "','" + //P_CARD_FLAG
        '' + "','" + //PCARD_NBR
        '' + "','" + //VAL_DIV_DATE
        0 + "','" + //POV_GTIN_USED
        0 + "','" + //GTIN_ITEM_SYNC
        '' + "','" + //AP_GLN_NBR
        '' + "','" + //PO_GLN_NBR
        '' + "','" + //SEC_WTH_EXEMPT
        '' + "','" + //SEC_WTH_CODE1
        '' + "','" + //SEC_WTH_CODE2
        '' + "','" + //SEC_WTH_CODE3
        '' + "','" + //VALID_CERT_DT
        '' + "','" + //FOR_ECON_CODE
        '' + "','" + //CARRIER_FLAG
        '' + "','" + //BANK_CODE
        '' + "','" + //BANK_ID
        '' + "','" + //ASSIGNMENT_NBR
        '' + "','" + //DEBITING_SIGN
        '' + "','" + //INTM_BANK_CODE
        '' + "','" + //INTM_BANK_ID
        '' + "','" + //INTM_ASSIGN_NO
        '' + "','" + //INTM_DEBITING
        '' + "','" + //CHK_DIG_TYPE
        '' + "','" + //SEPA_FLAG
        '' + "'" + //INTM_SEPA_FLAG
        ")"
    _logger.info("TDIC_LawsonHelper#getVenMastCreateStringLocationRecord() - Create String: " + createString)
    _logger.info("TDIC_LawsonHelper#getVenMastCreateStringLocationRecord() - Exiting")
    return createString
  }

  /**
   * US556
   * 02/10/2015 Shane Murphy
   *
   * Returns the create string for a contact on the APCVENMAST table. All 204 fields on this table are nonnullable
   */
  @Param("aContact", "Contact to retrieve create string for")
  @Returns("APCVENMAST Update String")
  function getVenMastCreateStringVendorRecord(aContact : ABContact) : String {
    _logger.info("TDIC_LawsonHelper#getVenMastCreateStringVendorRecord() - Entering")
    var formattedLines = aContact typeis ABCompany ?
        getFormattedComanyAddress(_truncLength, 5, aContact) :
        getFormattedPersonAddress(_truncLength, 5, (aContact as ABPerson))
    var createString = "INSERT INTO APCVENMAST (" +
        "\"VENDOR_GROUP\"," + "\"VENDOR\"," + "\"OLD_VENDOR\"," + "\"LOCATION_CODE\"," + "\"LOC_TYPE\"," + "\"VEN_CLASS\"," + "\"VENDOR_VNAME\"," + "\"VENDOR_SNAME\"," + "\"VENDOR_CONTCT\"," + "\"ADDR1\"," +
        "\"ADDR2\"," + "\"ADDR3\"," + "\"ADDR4\"," + "\"CITY_ADDR5\"," + "\"STATE_PROV\"," + "\"POSTAL_CODE\"," + "\"COUNTY\"," + "\"COUNTRY\"," + "\"REGION\"," + "\"COUNTRY_CODE\"," +
        "\"NORM_REMIT\"," + "\"NORM_PURCH\"," + "\"REMIT_TO_CODE\"," + "\"PURCH_FR_LOC\"," + "\"PAY_VENDOR\"," + "\"VENDOR_STATUS\"," + "\"VEN_PRIORITY\"," + "\"PHONE_PREFIX\"," + "\"PHONE_NUM\"," + "\"PHONE_EXT\"," +
        "\"FAX_PREFIX\"," + "\"FAX_NUM\"," + "\"FAX_EXT\"," + "\"TELEX_NUM\"," + "\"TERM_CODE\"," + "\"INV_CURRENCY\"," + "\"BAL_CURRENCY\"," + "\"BANK_CURRENCY\"," + "\"CURR_RECALC\"," + "\"SEP_CHK_FLAG\"," +
        "\"TAX_ID\"," + "\"TAX_CODE\"," + "\"HLD_CODE\"," + "\"DIST_CODE\"," + "\"ACCR_CODE\"," + "\"BANK_INST_CODE\"," + "\"CASH_CODE\"," + "\"BANK_ENTITY\"," + "\"VBANK_ACCT_NO\"," + "\"VBANK_IDENT\"," +
        "\"RIBKEY\"," + "\"VBANK_ACCT_TP\"," + "\"INCOME_CODE\"," + "\"INCOME_WH_FLG\"," + "\"EDI_NBR\"," + "\"ACH_PRENOT\"," + "\"MAX_INV_AMT\"," + "\"ORIGIN_DATE\"," + "\"USER_NAME_01\"," + "\"USER_NAME_02\"," +
        "\"USER_NAME_03\"," + "\"USER_NAME_04\"," + "\"USER_NAME_05\"," + "\"USER_NAME_06\"," + "\"CUST_GROUP\"," + "\"CUSTOMER\"," + "\"LEGAL_NAME\"," + "\"ACTIVITY\"," + "\"ACCT_CATEGORY\"," + "\"DISCOUNT_CODE\"," +
        "\"PRIME_RATE\"," + "\"INVOICE_GROUP\"," + "\"ERS_CAPABLE\"," + "\"INVC_REF_TYPE\"," + "\"EDI_AUTO_REL\"," + "\"AUTH_CODE\"," + "\"CHARGE_CODE\"," + "\"PMT_CAT_CODE\"," + "\"NORM_EXP_CODE\"," + "\"PMT_FORM\"," +
        "\"SWIFT_ID\"," + "\"PROC_GRP\"," + "\"MATCH_TABLE\"," + "\"HANDLING_CODE\"," + "\"DISC_CALC_DATE\"," + "\"ENCLOSURE\"," + "\"CREATE_POD_FL\"," + "\"REQ_MATCH_REF\"," + "\"POOL_OPTION\"," + "\"HOLD_INSP_FLAG\"," +
        "\"WRITE_OFF_AMT\"," + "\"VEN_CLAIM_TYPE\"," + "\"CLAIM_HOLD_CD\"," + "\"CB_HOLD_CODE\"," + "\"CB_MINIMUM_AMT\"," + "\"REPLACE_GOODS\"," + "\"SHIP_OR_HOLD\"," + "\"ERS_HANDLING\"," + "\"E_MAIL_ADDRESS\"," + "\"URL_ADDR\"," +
        "\"VEND_ACCT\"," + "\"LANGUAGE_CODE\"," + "\"TAX_USAGE_CD\"," + "\"VAT_REG_CTRY\"," + "\"VAT_REG_NBR\"," + "\"VALIDATE_PO\"," + "\"REQUIRE_PO\"," + "\"POV_BUYER_CODE\"," + "\"POV_OPEN_PO_LM\"," + "\"POV_OPEN_PO_AM\"," +
        "\"POV_LEADTIME\"," + "\"POV_EDI_NUMBER\"," + "\"POV_FAX_PREFIX\"," + "\"POV_FAX_EXT\"," + "\"POV_FAX_NUMBER\"," + "\"POV_MAX_ORD_AM\"," + "\"POV_FREIGHT_TR\"," + "\"POV_SHIP_VIA\"," + "\"POV_FOB_CODE\"," + "\"POV_NORM_DEL\"," +
        "\"POV_OSHIP_TPCT\"," + "\"POV_USHIP_TPCT\"," + "\"POV_HIN_NBR\"," + "\"POV_ZOHSHIP_FL\"," + "\"POV_ZUHSHIP_FL\"," + "\"POV_CAN_ALL_BO\"," + "\"POV_RMA_REQ\"," + "\"POV_RMA_DOC_RQ\"," + "\"POV_OV_SHP_VIA\"," + "\"POV_REQ_LOCLVL\"," +
        "\"POV_REQ_CONLVL\"," + "\"POV_ISSUE_MTHD\"," + "\"POV_REV_ISS_MT\"," + "\"POV_REV_EDI_NB\"," + "\"POV_REV_EDI_TR\"," + "\"POV_REV_INCL\"," + "\"POV_VEN_CONT\"," + "\"POV_PHONE_PREF\"," + "\"POV_PHONE_NUM\"," + "\"POV_PHONE_EXT\"," +
        "\"POV_VEND_ACCT\"," + "\"POV_PO_CODE\"," + "\"POV_UPD_PO_CST\"," + "\"POV_USR_FLD_01\"," + "\"POV_USR_FLD_02\"," + "\"POV_USR_FLD_03\"," + "\"POV_USR_FLD_04\"," + "\"POV_USR_FLD_05\"," + "\"MBL_INT_PREFIX\"," + "\"MOBILE_NUM\"," +
        "\"MOBILE_EXT\"," + "\"PAY_IMM_FLAG\"," + "\"DIVERSE_CODE\"," + "\"INTM_BANK_ENT\"," + "\"INTM_BANK_ACCT\"," + "\"INTM_BANK_IDNT\"," + "\"INTM_BANK_CURR\"," + "\"INTM_PRENOT\"," + "\"INTM_SWIFT_ID\"," + "\"INTM_PMT_CAT\"," +
        "\"INTM_NORM_EXP\"," + "\"INTM_PMT_FORM\"," + "\"INTM_CHRG_CD\"," + "\"INTM_RIBKEY\"," + "\"INTM_CRS_IDENT\"," + "\"FLOAT_DAYS\"," + "\"VN_BLD_SOC_REF\"," + "\"VN_BLD_ACCT_NM\"," + "\"IN_BLD_SOC_REF\"," + "\"IN_BLD_ACCT_NM\"," +
        "\"MTCH_PREPAY_FL\"," + "\"MTCH_PREPAY_MT\"," + "\"BUILD_SOC_REF\"," + "\"BUILD_ACCT_NM\"," + "\"MIN_ORD_WEIGHT\"," + "\"UNLOADING_PORT\"," + "\"LOADING_PORT\"," + "\"WORKFLOW_GROUP\"," + "\"RULE_GROUP\"," + "\"CROSS_IDENT\"," +
        "\"GIRO_NUMBER\"," + "\"CR_CARD_NUMBER\"," + "\"BANK_INSTRUCT1\"," + "\"BANK_INSTRUCT2\"," + "\"BANK_INSTRUCT3\"," + "\"BANK_INSTRUCT4\"," + "\"LOC_ID_PRT\"," + "\"PUNCHOUT_CHG_F\"," + "\"EMAIL_ADDRESS\"," + "\"REORDER_SUN_FL\"," +
        "\"REORDER_MON_FL\"," + "\"REORDER_TUE_FL\"," + "\"REORDER_WED_FL\"," + "\"REORDER_THU_FL\"," + "\"REORDER_FRI_FL\"," + "\"REORDER_SAT_FL\"," + "\"DIVERSE_CODE5\"," + "\"P_CARD_FLAG\"," + "\"PCARD_NBR\"," + "\"VAL_DIV_DATE\"," +
        "\"POV_GTIN_USED\"," + "\"GTIN_ITEM_SYNC\"," + "\"AP_GLN_NBR\"," + "\"PO_GLN_NBR\"," + "\"SEC_WTH_EXEMPT\"," + "\"SEC_WTH_CODE1\"," + "\"SEC_WTH_CODE2\"," + "\"SEC_WTH_CODE3\"," + "\"VALID_CERT_DT\"," + "\"FOR_ECON_CODE\"," +
        "\"CARRIER_FLAG\"," + "\"BANK_CODE\"," + "\"BANK_ID\"," + "\"ASSIGNMENT_NBR\"," + "\"DEBITING_SIGN\"," + "\"INTM_BANK_CODE\"," + "\"INTM_BANK_ID\"," + "\"INTM_ASSIGN_NO\"," + "\"INTM_DEBITING\"," + "\"CHK_DIG_TYPE\"," +
        "\"SEPA_FLAG\"," + "\"INTM_SEPA_FLAG\")" + " VALUES ('" +
        fillNull(vendorGroup) + "','" + //VENDOR_GROUP
        '' + "','" + //VENDOR
        aContact.PublicID + "','" + //OLD_VENDOR
        '' + "','" + //LOCATION_CODE
        '' + "','" + //LOC_TYPE
        fillNull(venClass) + "','" + //VEN_CLASS
        escapeForSql(limitLength(fillNull(formattedLines[0]), 30)).toUpperCase() + "','" + //VENDOR_VNAME
        '' + "','" + //VENDOR_SNAME
        '' + "','" + //VENDOR_CONTCT
        escapeForSql(limitLength(fillNull(formattedLines[1]), 30)).toUpperCase() + "','" + //ADDR1
        escapeForSql(limitLength(fillNull(formattedLines[2]), 30)).toUpperCase() + "','" + //ADDR2
        escapeForSql(limitLength(fillNull(formattedLines[3]), 30)).toUpperCase() + "','" + // ADDR3
        escapeForSql(limitLength(fillNull(formattedLines[4]), 30)).toUpperCase() + "','" + //ADDR4
        escapeForSql(limitLength(fillNull(aContact.PrimaryAddress.City), 18)).toUpperCase() + "','" + //CITY_ADDR5
        fillNull(aContact.PrimaryAddress.State as String).toUpperCase() + "','" + //STATE_PROV
        fillNull(aContact.PrimaryAddress.PostalCode).toUpperCase() + "','" + //POSTAL_CODE
        escapeForSql(limitLength(fillNull(aContact.PrimaryAddress.County), 25)).toUpperCase() + "','" + //COUNTY
        fillNull(aContact.PrimaryAddress.Country.Code).toUpperCase() + "','" + //COUNTRY
        '' + "','" + //REGION
        '' + "','" + //COUNTRY_CODE
        '' + "','" + //NORM_REMIT
        '' + "','" + //NORM_PURCH
        '' + "','" + //REMIT_TO_CODE
        '' + "','" + //PURCH_FR_LOC
        '' + "','" + //PAY_VENDOR
        '' + "','" + //VENDOR_STATUS
        0 + "','" + //VEN_PRIORITY
        '' + "','" + //PHONE_PREFIX
        '' + "','" + //PHONE_NUM
        '' + "','" + //PHONE_EXT
        '' + "','" + //FAX_PREFIX
        '' + "','" + //FAX_NUM
        '' + "','" + //FAX_EXT
        '' + "','" + //TELEX_NUM
        'NET0' + "','" + //TERM_CODE
        '' + "','" + //INV_CURRENCY
        '' + "','" + //BAL_CURRENCY
        '' + "','" + //BANK_CURRENCY
        '' + "','" + //CURR_RECALC
        '' + "','" + //SEP_CHK_FLAG
        fillNull(aContact.TaxID) + "','" + //TAX_ID
        '' + "','" + //TAX_CODE
        '' + "','" + //HLD_CODE
        '' + "','" + //DIST_CODE
        '' + "','" + //ACCR_CODE
        '' + "','" + //BANK_INST_CODE
        '' + "','" + //CASH_CODE
        '' + "','" + //BANK_ENTITY
        '' + "','" + //VBANK_ACCT_NO
        '' + "','" + //VBANK_IDENT
        0 + "','" + //RIBKEY
        '' + "','" + //VBANK_ACCT_TP
        '' + "','" + //INCOME_CODE
        '' + "','" + //INCOME_WH_FLG
        '' + "','" + //EDI_NBR
        '' + "','" + //ACH_PRENOT
        0 + "','" + //MAX_INV_AMT
        fillNull(DateUtil.currentDate().toSQLDate() as String) + "','" + //ORIGIN_DATE
        fillNull(aContact.PublicID) + "','" + //USER_NAME_01 = PublicID
        '' + "','" + //USER_NAME_02 = TaxID (FEIN/SSN)
        '' + "','" + //USER_NAME_03 = PersonFEIN_TDIC
        '' + "','" + //USER_NAME_04 = SSN
        '' + "','" + //USER_NAME_05 = STax
        '' + "','" + //USER_NAME_06 = BureauID
        '' + "','" + //CUST_GROUP
        '' + "','" + //CUSTOMER
        escapeForSql(limitLength(fillNull(getDisplayName(aContact)).replaceAll("[^0-9 A-Z a-z]", ""), 80)).toUpperCase() + "','" + //LEGAL_NAME
        '' + "','" + //ACTIVITY
        '' + "','" + //ACCT_CATEGORY
        '' + "','" + //DISCOUNT_CODE
        0 + "','" + //PRIME_RATE
        '' + "','" + //INVOICE_GROUP
        '' + "','" + //ERS_CAPABLE
        '' + "','" + //INVC_REF_TYPE
        '' + "','" + //EDI_AUTO_REL
        '' + "','" + //AUTH_CODE
        '' + "','" + //CHARGE_CODE
        '' + "','" + //PMT_CAT_CODE
        '' + "','" + //NORM_EXP_CODE
        '' + "','" + //PMT_FORM
        '' + "','" + //SWIFT_ID
        '' + "','" + //PROC_GRP
        '' + "','" + //MATCH_TABLE
        '' + "','" + //HANDLING_CODE
        '' + "','" + //DISC_CALC_DATE
        '' + "','" + //ENCLOSURE
        '' + "','" + //CREATE_POD_FL
        '' + "','" + //REQ_MATCH_REF
        0 + "','" + //POOL_OPTION
        '' + "','" + //HOLD_INSP_FLAG
        0 + "','" + //WRITE_OFF_AMT
        '' + "','" + //VEN_CLAIM_TYPE
        '' + "','" + //CLAIM_HOLD_CD
        '' + "','" + //CB_HOLD_CODE
        0 + "','" + //CB_MINIMUM_AMT
        '' + "','" + //REPLACE_GOODS
        '' + "','" + //SHIP_OR_HOLD
        '' + "','" + //ERS_HANDLING
        '' + "','" + //E_MAIL_ADDRESS
        '' + "','" + //URL_ADDR
        '' + "','" + //VEND_ACCT
        '' + "','" + //LANGUAGE_CODE
        '' + "','" + //TAX_USAGE_CD
        '' + "','" + //VAT_REG_CTRY
        '' + "','" + //VAT_REG_NBR
        '' + "','" + //VALIDATE_PO
        '' + "','" + //REQUIRE_PO
        '' + "','" + //POV_BUYER_CODE
        0 + "','" + //POV_OPEN_PO_LM
        0 + "','" + //POV_OPEN_PO_AM
        0 + "','" + //POV_LEADTIME
        '' + "','" + //POV_EDI_NUMBER
        '' + "','" + //POV_FAX_PREFIX
        '' + "','" + //POV_FAX_EXT
        '' + "','" + //POV_FAX_NUMBER
        0 + "','" + //POV_MAX_ORD_AM
        '' + "','" + //POV_FREIGHT_TR
        '' + "','" + //POV_SHIP_VIA
        '' + "','" + //POV_FOB_CODE
        '' + "','" + //POV_NORM_DEL
        0 + "','" + //POV_OSHIP_TPCT
        0 + "','" + //POV_USHIP_TPCT
        '' + "','" + //POV_HIN_NBR
        '' + "','" + //POV_ZOHSHIP_FL
        '' + "','" + //POV_ZUHSHIP_FL
        '' + "','" + //POV_CAN_ALL_BO
        '' + "','" + //POV_RMA_REQ
        '' + "','" + //POV_RMA_DOC_RQ
        '' + "','" + //POV_OV_SHP_VIA
        '' + "','" + //POV_REQ_LOCLVL
        '' + "','" + //POV_REQ_CONLVL
        '' + "','" + //POV_ISSUE_MTHD
        '' + "','" + //POV_REV_ISS_MT
        '' + "','" + //POV_REV_EDI_NB
        '' + "','" + //POV_REV_EDI_TR
        '' + "','" + //POV_REV_INCL
        '' + "','" + //POV_VEN_CONT
        '' + "','" + //POV_PHONE_PREF
        '' + "','" + //POV_PHONE_NUM
        '' + "','" + //POV_PHONE_EXT
        '' + "','" + //POV_VEND_ACCT
        '' + "','" + //POV_PO_CODE
        0 + "','" + //POV_UPD_PO_CST
        '' + "','" + //POV_USR_FLD_01
        '' + "','" + //POV_USR_FLD_02
        '' + "','" + //POV_USR_FLD_03
        '' + "','" + //POV_USR_FLD_04
        '' + "','" + //POV_USR_FLD_05
        '' + "','" + //MBL_INT_PREFIX
        '' + "','" + //MOBILE_NUM
        '' + "','" + //MOBILE_EXT
        '' + "','" + //PAY_IMM_FLAG
        '' + "','" + //DIVERSE_CODE
        '' + "','" + //INTM_BANK_ENT
        '' + "','" + //INTM_BANK_ACCT
        '' + "','" + //INTM_BANK_IDNT
        '' + "','" + //INTM_BANK_CURR
        '' + "','" + //INTM_PRENOT
        '' + "','" + //INTM_SWIFT_ID
        '' + "','" + //INTM_PMT_CAT
        '' + "','" + //INTM_NORM_EXP
        '' + "','" + //INTM_PMT_FORM
        '' + "','" + //INTM_CHRG_CD
        0 + "','" + //INTM_RIBKEY
        '' + "','" + //INTM_CRS_IDENT
        0 + "','" + //FLOAT_DAYS
        '' + "','" + //VN_BLD_SOC_REF
        '' + "','" + //VN_BLD_ACCT_NM
        '' + "','" + //IN_BLD_SOC_REF
        '' + "','" + //IN_BLD_ACCT_NM
        '' + "','" + //MTCH_PREPAY_FL
        0 + "','" + //MTCH_PREPAY_MT
        '' + "','" + //BUILD_SOC_REF
        '' + "','" + //BUILD_ACCT_NM
        0 + "','" + //MIN_ORD_WEIGHT
        '' + "','" + //UNLOADING_PORT
        '' + "','" + //LOADING_PORT
        '' + "','" + //WORKFLOW_GROUP
        '' + "','" + //RULE_GROUP
        '' + "','" + //CROSS_IDENT
        '' + "','" + //GIRO_NUMBER
        '' + "','" + //CR_CARD_NUMBER
        '' + "','" + //BANK_INSTRUCT1
        '' + "','" + //BANK_INSTRUCT2
        '' + "','" + //BANK_INSTRUCT3
        '' + "','" + //BANK_INSTRUCT4
        0 + "','" + //LOC_ID_PRT
        0 + "','" + //PUNCHOUT_CHG_F
        '' + "','" + //EMAIL_ADDRESS
        0 + "','" + //REORDER_SUN_FL
        0 + "','" + //REORDER_MON_FL
        0 + "','" + //REORDER_TUE_FL
        0 + "','" + //REORDER_WED_FL
        0 + "','" + //REORDER_THU_FL
        0 + "','" + //REORDER_FRI_FL
        0 + "','" + //REORDER_SAT_FL
        '' + "','" + //DIVERSE_CODE5
        0 + "','" + //P_CARD_FLAG
        '' + "','" + //PCARD_NBR
        '' + "','" + //VAL_DIV_DATE
        0 + "','" + //POV_GTIN_USED
        0 + "','" + //GTIN_ITEM_SYNC
        '' + "','" + //AP_GLN_NBR
        '' + "','" + //PO_GLN_NBR
        '' + "','" + //SEC_WTH_EXEMPT
        '' + "','" + //SEC_WTH_CODE1
        '' + "','" + //SEC_WTH_CODE2
        '' + "','" + //SEC_WTH_CODE3
        '' + "','" + //VALID_CERT_DT
        '' + "','" + //FOR_ECON_CODE
        '' + "','" + //CARRIER_FLAG
        '' + "','" + //BANK_CODE
        '' + "','" + //BANK_ID
        '' + "','" + //ASSIGNMENT_NBR
        '' + "','" + //DEBITING_SIGN
        '' + "','" + //INTM_BANK_CODE
        '' + "','" + //INTM_BANK_ID
        '' + "','" + //INTM_ASSIGN_NO
        '' + "','" + //INTM_DEBITING
        '' + "','" + //CHK_DIG_TYPE
        '' + "','" + //SEPA_FLAG
        '' + "'" + //INTM_SEPA_FLAG
        ")"
    _logger.info("TDIC_LawsonHelper#getVenMastCreateStringVendorRecord() - Create String: " + createString)
    _logger.info("TDIC_LawsonHelper#getVenMastCreateStringVendorRecord() - Exiting")
    return createString
  }

  /**
   * US556
   * 02/20/2015 Shane Murphy
   *
   * Truncates lines > truncLength characters long at the last space character.
   * Returns an array where each element can be used as a line in the address.
   */
  @Param("truncLength", "The max length of each array element")
  @Param("linesToFormat", "Lines to format")
  @Param("minNumLines", "The minimum number of entries in the array we need (To prevent NPE)")
  @Returns("String[] of address lines")
  function formatLines(truncLength : int, minNumLines : int, linesToFormat : ArrayList<String>) : String[] {
    var formattedLines = new ArrayList()
    var lineRemainder = ""
    for (line in linesToFormat) {
      line = fillNull(line)
      if (line.length <= 30) {
        formattedLines.add(line)
      } else {
        var split = getSplitPoint(truncLength, line)
        formattedLines.add(line.substring(0, split))
        lineRemainder = line.substring(split)
        if(lineRemainder.length > 30) {
          lineRemainder = lineRemainder.substring(0,30)
        }
        formattedLines.add(lineRemainder)
      }
    }

    while (formattedLines.size() < minNumLines) {
      formattedLines.add("")// add blank lines  to meet min required length
    }
    return formattedLines?.asArrayOf(String)
  }

  /**
   * US556
   * 02/20/2015 Shane Murphy
   *
   * Truncates lines >truncLength characters long at the last space character.
   * Returns an array where each element can be used as a line in the address.
   */
  @Param("truncLength", "The max length of each array element")
  @Param("lineToFormat", "Line to format")
  @Returns("The index at which to split the string")
  function getSplitPoint(truncLength : int, lineToFormat : String) : int {
    var first30Characters = lineToFormat.substring(0, truncLength)
    var remainingCharacters = lineToFormat.substring(truncLength)
    if (remainingCharacters.startsWith(" ") or first30Characters.endsWith(" ")) {
      return truncLength
    } else {
      var spaceSplitPoint = first30Characters.lastIndexOf(" ")
      return spaceSplitPoint > 0 ? spaceSplitPoint : truncLength
    }
  }

  /**
   * US556
   * 04/09/2015 Kesava Tavva
   *
   * Check for address field value changes for
   * AddressLine1, AddressLine2, AddressLine3, City, State, PostalCode, Country
   */
  @Param("truncLength", "The max length of each array element")
  @Param("lineToFormat", "Line to format")
  @Returns("The index at which to split the string")
  static function isContactNameTaxIDAddressChanged(contact : ABContact): Boolean {
    var result = contact.PrimaryAddress?.isFieldChanged("AddressLine1")
        || contact.PrimaryAddress?.isFieldChanged("AddressLine2")
        || contact.PrimaryAddress?.isFieldChanged("AddressLine3")
        || contact.PrimaryAddress?.isFieldChanged("City")
        || contact.PrimaryAddress?.isFieldChanged("State")
        || contact.PrimaryAddress?.isFieldChanged("PostalCode")
        || contact.PrimaryAddress?.isFieldChanged("Country")
        || contact.isFieldChanged("PrimaryAddress")
        || contact.isFieldChanged("Name")
        || contact.isFieldChanged("TaxID")
    if(result == false and contact typeis  ABPerson){
      if(contact.isFieldChanged("FirstName") || contact.isFieldChanged("LastName"))
        result = true
    }
    _logger.info("TDIC_LawsonHelper#isAddressChanged() - ${contact} - ${result}")
    return result
  }

  private static function formatVendorNumber(vendorNumber: String, publicID: String): String {
    return (vendorNumber.HasContent) ? String.format("%9s", {vendorNumber}) : "${VENDOR_NUMBER_NOT_AVAILABLE}#${publicID}#"
  }

  function getDisplayName(aContact : ABContact) : String {
    var displayName : String  = ""
    if(aContact.DisplayName.length < 80){
      displayName = aContact.DisplayName
    }
    else {
      displayName = aContact.DisplayName.substring(0,80)
    }
    return displayName
  }

  /**
   *
   * 03/07/2016 Sunnihith Bojedla
   * Check for vendor number change
   *
   */
  static function isVendorNumberChanged(contact : ABContact): Boolean {
    if(contact.isFieldChanged("VendorNumber") and contact.VendorNumber == "99") {
      return true
    }
    else {
      return false
    }
  }
}