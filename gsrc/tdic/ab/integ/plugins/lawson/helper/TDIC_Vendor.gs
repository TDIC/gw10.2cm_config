package tdic.ab.integ.plugins.lawson.helper

uses gw.pl.logging.LoggerCategory
uses tdic.util.cache.CacheManager
uses com.tdic.util.database.DatabaseManager

uses java.io.IOException
uses java.lang.Exception
uses java.sql.Connection
uses java.sql.SQLException
uses gw.api.database.Query
uses java.sql.ResultSet

/**
 * US1045
 * 03/12/2015 Sam Kiriaki, Kesava Tavva
 *
 * Utility class for retrieving vendor number from Lawson with the contact's taxid and update
 * contact's vendor number in Contact Center with the retrieved vendor number.
 */
class TDIC_Vendor {
  private static var _logger = LoggerCategory.TDIC_INTEGRATION

  /*
   * This function is called when while creating a payment, the vendor number is not available on a contact entity
   * It's called with the contact taxId that is used to retrieve from Lawson the (newly created) vendor number
   * The extracted vendor number is then:
   *   - returned to the calling program for immediate use
   *   - added to the contact entity for future use
   */
  @Param("taxId","String value used to retrieve VendorNumber from Lawson database.")
  @Returns("String, Value of Vendor number retrieved from Lawson database")
  @Throws(IOException, "If there are any IO errors")
  @Throws(SQLException, "If there are JDBC issues such as connectivity, reading data")
  @Throws(Exception, "If there are any other errors")
  public function getVendorNumberFromLawson(taxId: String): String {
    _logger.debug("Vendor#getVendorNumberFromLawson(${taxId}) - Entering")

    var _con: Connection
    var _resultSet : ResultSet
    var _vendorNumber: String

    try {
      if(taxId == null){
        _logger.debug("Vendor#getVendorNumberFromLawson(${taxId}) - taxId can't be null")
        _logger.debug("Vendor#getVendorNumberFromLawson(${taxId}) - Exiting")
        throw new Exception("TaxID can't be null for retrieving VendorNumber from Lawson.")
      }

      var _db = CacheManager.AppDatabaseCache.get("Lawson")
      _con = DatabaseManager.getConnection(_db.Host, _db.Instance, _db.Port, _db.DbName, _db.UserName,_db.Password)
      var _sqlStmt = "SELECT VENDOR FROM APVENMAST WHERE TAX_ID = '${taxId}'"
      _resultSet = DatabaseManager.executeQuery(_con, _sqlStmt)
      if (_resultSet.next()) {
        _vendorNumber = _resultSet.getString("VENDOR")
        // Update the contact with the retrieved vendor number
        //updateContactManager(taxId, _vendorNumber)
      } else {
        _vendorNumber = null
        _logger.debug("Vendor#getVendorNumberFromLawson(${taxId}) - Unable to find vendor ID for vendor with tax ID= " + taxId)
      }
      _logger.debug("Vendor#getVendorNumberFromLawson(${taxId}) - VendorNumber: ${_vendorNumber}")
      _logger.debug("Vendor#getVendorNumberFromLawson() - Exiting")
      return _vendorNumber

    } catch (io:IOException) {
      _logger.error("Vendor#getVendorNumberFromLawson(${taxId}) - IOException= ", io)
      throw(io)
    } catch (sql:SQLException) {
      _logger.error("Vendor#getVendorNumberFromLawson(${taxId}) - SQLException= ", sql)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("Vendor#getVendorNumberFromLawson(${taxId}) - Exception= ", e)
      throw(e)
    } finally {
      try {
        if (_resultSet != null) _resultSet.close()
        if (_con != null) _con.close()
      } catch (fe:Exception) {
        _logger.error("Vendor#getVendorNumberFromLawson(${taxId}) - finally Exception= " + fe)
      }
    }
  }

  /*
   * This function updates contact's vendor number with the details retrieved from Lawson database.
   */
  @Param("taxId","String value used to find contact in ContactManager.")
  @Param("vendorNumber", "String value used to update Contact's VendorNumber in ContactManager")
  @Returns("boolean, Boolean result of update process.")
  @Throws(Exception, "If taxId is null or If there are any other errors")
  public function updateContactManager(taxId: String, vendorNumber: String): boolean {
    _logger.debug("Vendor#updateContactManager(${taxId}, ${vendorNumber}) - Entering")
    try {

      if(taxId == null){
        _logger.debug("Vendor#updateContactManager(${taxId}, ${vendorNumber}) - taxId can't be null")
        _logger.debug("Vendor#updateContactManager(${taxId}, ${vendorNumber}) - Exiting")
        throw new Exception("TaxID can't be null for updating VendorNumber for a Contact in ContactManager.")
      }

      var contact = Query.make(ABContact).compare("TaxID",Equals,taxId).select().AtMostOneRow
      if(contact == null){
        _logger.debug("Vendor#updateContactManager(${taxId}, ${vendorNumber}) - Unable to find Contact with tax ID=${taxId}")
        _logger.debug("Vendor#updateContactManager(${taxId}, ${vendorNumber}) - Exiting")
        return Boolean.FALSE
      }else{
        gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
          contact = bundle.add(contact)
          contact.VendorNumber = vendorNumber
        }, "su")
      }
    } catch(e : Exception){
      throw (e)
    }
    _logger.debug("Vendor#updateContactManager(${taxId}, ${vendorNumber}) - Exiting")
    return Boolean.TRUE
  }
}