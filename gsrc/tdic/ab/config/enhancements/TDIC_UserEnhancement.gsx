package tdic.ab.config.enhancements

/**
 * DE102
 * 02/04/2015 Rob Kelly
 *
 * Enhancement providing a method to initialize the PrimaryAddress for a User.
 */
enhancement TDIC_UserEnhancement : entity.User {

  /**
   * Creates a new PrimaryAddress for this User and initializes the State column to CA.
   */
  function initPrimaryAddress() {

    this.Contact.PrimaryAddress = new Address()
    this.Contact.PrimaryAddress.State = typekey.State.TC_CA
  }
}
