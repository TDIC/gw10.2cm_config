package tdic.util.lawsonap

uses gw.api.databuilder.AddressBuilder
uses tdic.ab.integ.plugins.lawson.helper.TDIC_LawsonHelper

@gw.testharness.ServerTest
class TDIC_LawsonHelperTest extends gw.testharness.TestBase {
  var _helper: TDIC_LawsonHelper
//  var addresss = new AddressBuilder().withAddressLine1("12345 12345 12345 12345 12345 ")
  /**
   * This method is used to initialize instances that are required for each test case execution
   */
  override function beforeMethod() {
    _helper = new TDIC_LawsonHelper()
  }

  function testLineFormat_AddressThreeLinesOnly() {
    var address = new AddressBuilder().withAddressLine1("this is thirty characters long")
        .withAddressLine2("this is thirty characters long")
        .withAddressLine3("this is thirty characters long")
        .create()
    var formattedAddress =_helper.formatLines(30, 4, {address.AddressLine1,address.AddressLine2,address.AddressLine3})
    assertEquals("", formattedAddress[3])
  }

  function testLineFormat_AddressFourLinesOnly() {
    var address = new AddressBuilder().withAddressLine1("12345 12345 12345 12345 12345 12345")
        .withAddressLine2("12345 12345 12345 12345 12345")
        .withAddressLine3("12345 12345 12345 12345 12345")
        .create()
    var formattedAddress =_helper.formatLines(30, 5, {address.AddressLine1,address.AddressLine2,address.AddressLine3})
    assertEquals("", formattedAddress[4])
  }

  function testLineFormat_AddressExpectedFourthLineValue() {
    var address = new AddressBuilder().withAddressLine1("12345 12345 12345 12345 12345 12345")
        .withAddressLine2("12345 12345 12345 12345 12345")
        .withAddressLine3("12345 12345 12345 12345 12345")
        .create()
    var formattedAddress =_helper.formatLines(30, 4, {address.AddressLine1,address.AddressLine2,address.AddressLine3})
    assertEquals("12345", formattedAddress[3])
  }

  function testLineFormat_CompanyExpectedSecondLineOverflowValue() {
    var address = new AddressBuilder().withAddressLine1("12345 12345 12345 12345 12345 12345")
        .withAddressLine2("12345 12345 12345 12345 12345")
        .withAddressLine3("12345 12345 12345 12345 12345")
        .create()
    var companyName= "ThisCompanyNameIsThirtySixCharacters"
    var formattedAddress =_helper.formatLines(30, 2, {companyName, address.AddressLine1,address.AddressLine2,address.AddressLine3})
    assertEquals("acters", formattedAddress[1].substring(0,6))
  }

  function testLineFormat_CompanyExpectedSecondLineValue() {
    var address = new AddressBuilder().withAddressLine1("12345 12345 12345 12345")
        .withAddressLine2("12345 12345 12345 12345 12345")
        .withAddressLine3("12345 12345 12345 12345 12345")
        .create()
    var companyName= "ThisCompanyNameHas30Characters"
    var formattedAddress =_helper.formatLines(30, 2, {companyName, address.AddressLine1,address.AddressLine2,address.AddressLine3})
    assertEquals(address.AddressLine1, formattedAddress[1])
  }
}